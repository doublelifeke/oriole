package com.oriole.admin.controller;

import com.oriole.admin.feign.ISysDeptService;
import com.oriole.common.annotation.OperationLog;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.common.enums.BusinessType;
import com.oriole.common.util.OrioleStringUtils;
import com.oriole.entity.sys.SysConfig;
import com.oriole.entity.sys.SysDept;
import com.oriole.entity.sys.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/8 20:50
 * Description:
 */
@Api(tags = {"系统部门-SysDeptController"})
@Controller
@RequestMapping("/sysDept")
public class SysDeptController {

    @Resource
    private ISysDeptService sysDeptService;

    @OperationLog(title = "系统部门-首页", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统部门-首页")
    @GetMapping("/index")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("/sysDept/index");
        return modelAndView;
    }

    @OperationLog(title = "系统部门-获取系统部门列表", businessType = BusinessType.SELECT)
    @ApiOperation(value = "系统部门-获取系统部门列表")
    @ResponseBody
    @GetMapping("/findAll")
    @Secured("ROLE_SUPER")
    public ResultModel<List<SysDept>> findAll() {
        return sysDeptService.findAll();
    }

    @OperationLog(title = "系统部门-获取系统部门列表分页", businessType = BusinessType.SELECT)
    @ApiOperation(value = "系统部门-获取系统部门列表分页")
    @ResponseBody
    @GetMapping("/findPage")
    @Secured("ROLE_SUPER")
    public ResultModel<Object> findPage(OriolePage oriolePage) {
        ResultModel<Object> resultModel = sysDeptService.findPage(oriolePage);
        return ResultModel.success(PageableUtil.setResultPage(resultModel));
    }

    @OperationLog(title = "系统部门-新增页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统部门-新增页面")
    @GetMapping("/toSave")
    public ModelAndView toSave() {
        ModelAndView modelAndView = new ModelAndView("/sysDept/toSave");
        return modelAndView;
    }

    @OperationLog(title = "系统部门-保存部门", businessType = BusinessType.INSERT)
    @ApiOperation(value = "系统部门-保存部门")
    @ResponseBody
    @PostMapping("/save")
    @Secured("ROLE_SUPER")
    public ResultModel<SysDept> save(@Valid @RequestBody SysDept sysDept) {
        return sysDeptService.save(sysDept);
    }

    @OperationLog(title = "系统部门-查看页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统部门-查看页面")
    @GetMapping("/toDetail/{id}")
    public ModelAndView toDetail(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/sysDept/toDetail");
        SysDept sysDept = sysDeptService.findById(id).getData();
        modelAndView.addObject("sysDept", sysDept);
        return modelAndView;
    }

    @OperationLog(title = "系统部门-编辑页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统部门-编辑页面")
    @GetMapping("/toUpdate/{id}")
    public ModelAndView toUpdate(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/sysDept/toUpdate");
        SysDept sysDept = sysDeptService.findById(id).getData();
        modelAndView.addObject("sysDept", sysDept);
        return modelAndView;
    }

    @OperationLog(title = "系统部门-修改部门", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "系统部门-修改部门")
    @ResponseBody
    @PutMapping("/update")
    @Secured("ROLE_SUPER")
    public ResultModel<SysDept> update(@Valid @RequestBody SysDept sysDept) {
        return sysDeptService.update(sysDept);
    }

    @OperationLog(title = "系统部门-删除部门", businessType = BusinessType.DELETE)
    @ApiOperation(value = "系统部门-删除部门")
    @ResponseBody
    @DeleteMapping("/delete/{id}")
    @Secured("ROLE_SUPER")
    public ResultModel<Object> delete(@PathVariable("id") Long id) {
        return sysDeptService.deleteById(id);
    }

}
