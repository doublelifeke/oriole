package com.oriole.admin.controller;

import com.oriole.admin.feign.ISysOperationLogService;
import com.oriole.common.annotation.OperationLog;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.common.enums.BusinessType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/8 21:56
 * Description:
 */
@Api(tags = {"操作日志-SysOperationLogController"})
@Controller
@RequestMapping("/sysOperationLog")
public class SysOperationLogController {

    @Resource
    private ISysOperationLogService sysOperationLogService;

    @OperationLog(title = "操作日志-进入首页", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "操作日志-进入首页")
    @GetMapping("/index")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("/sysOperationLog/index");
        return modelAndView;
    }

    @OperationLog(title = "操作日志-获取列表分页", businessType = BusinessType.SELECT)
    @ApiOperation(value = "操作日志-获取列表分页")
    @ResponseBody
    @GetMapping("/findPage")
    public ResultModel<Object> findPage(OriolePage oriolePage) {
        ResultModel<Object> resultModel = sysOperationLogService.findPage(oriolePage);
        return ResultModel.success(PageableUtil.setResultPage(resultModel));
    }

    @OperationLog(title = "操作日志-根据ID删除操作日志", businessType = BusinessType.DELETE)
    @ApiOperation(value = "操作日志-根据ID删除操作日志")
    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public ResultModel<Object> deleteById(@PathVariable("id") Long id) {
        return sysOperationLogService.deleteById(id);
    }

}
