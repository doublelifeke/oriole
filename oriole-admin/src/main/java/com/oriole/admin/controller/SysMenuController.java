package com.oriole.admin.controller;

import com.oriole.admin.feign.ISysMenuService;
import com.oriole.common.annotation.OperationLog;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.enums.BusinessType;
import com.oriole.entity.sys.SysMenu;
import com.oriole.entity.vo.LayuiTreeSysMenu;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/11 23:20
 * Description:
 */
@Api(tags = {"系统菜单-SysMenuController"})
@Controller
@RequestMapping("/sysMenu")
public class SysMenuController extends BaseController {

    @Resource
    private ISysMenuService sysMenuService;

    @OperationLog(title = "系统菜单-首页", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统菜单-首页")
    @GetMapping("/index")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("/sysMenu/index");
        return modelAndView;
    }

    @OperationLog(title = "系统菜单-获取菜单列表-layui树形组件", businessType = BusinessType.SELECT)
    @ApiOperation(value = "系统菜单-获取菜单列表-layui树形组件")
    @ResponseBody
    @GetMapping("/findAll")
    @Secured("ROLE_SUPER")
    public ResultModel<List<LayuiTreeSysMenu>> findAll() {
        List<SysMenu> sysMenus = sysMenuService.findAll().getData();
        List<LayuiTreeSysMenu> layuiTreeSysMenus = getLayuiTreeSysMenus(sysMenus);
        return ResultModel.success(layuiTreeSysMenus);
    }

    @OperationLog(title = "系统菜单-新增子级菜单页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统菜单-新增子级菜单页面")
    @GetMapping("/toSave/{id}")
    public ModelAndView toSave(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/sysMenu/toSave");
        modelAndView.addObject("parentId", id);
        return modelAndView;
    }

    @OperationLog(title = "系统菜单-保存菜单", businessType = BusinessType.INSERT)
    @ApiOperation(value = "系统菜单-保存菜单")
    @ResponseBody
    @PostMapping("/save")
    @Secured("ROLE_SUPER")
    public ResultModel<SysMenu> save(@Valid @RequestBody SysMenu sysMenu) {
        return sysMenuService.save(sysMenu);
    }

    @OperationLog(title = "系统菜单-编辑页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统菜单-编辑页面")
    @GetMapping("/toUpdate/{id}")
    public ModelAndView toUpdate(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/sysMenu/toUpdate");
        SysMenu sysMenu = sysMenuService.findById(id).getData();
        modelAndView.addObject("sysMenu", sysMenu);
        return modelAndView;
    }

    @OperationLog(title = "系统菜单-修改菜单", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "系统菜单-修改菜单")
    @ResponseBody
    @PutMapping("/update")
    @Secured("ROLE_SUPER")
    public ResultModel<SysMenu> update(@Valid @RequestBody SysMenu sysMenu) {
        return sysMenuService.update(sysMenu);
    }

    @OperationLog(title = "系统菜单-删除菜单以及子级菜单", businessType = BusinessType.DELETE)
    @ApiOperation(value = "系统菜单-删除菜单以及子级菜单")
    @ResponseBody
    @DeleteMapping("/delete/{id}")
    @Secured("ROLE_SUPER")
    public ResultModel<Object> delete(@PathVariable("id") Long id) {
        return sysMenuService.deleteById(id);
    }

}
