package com.oriole.admin.controller;

import com.oriole.admin.feign.ISysFileService;
import com.oriole.common.annotation.OperationLog;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.enums.BusinessType;
import com.oriole.common.util.DateUtils;
import com.oriole.entity.sys.SysFile;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/31 14:06
 * Description:
 */
@Api(tags = {"系统文件-SysFileController"})
@Controller
@RequestMapping("/sysFile")
public class SysFileController extends BaseController {

    @Resource
    private ISysFileService sysFileService;

    @OperationLog(title = "系统文件-上传", businessType = BusinessType.INSERT)
    @ApiOperation(value = "系统文件-上传")
    @PostMapping("/upload")
    @ResponseBody
    public ResultModel<Object> upload(@RequestParam(value = "file") MultipartFile file) {
        if (file == null) {
            return ResultModel.success();
        } else {
            String originalName = file.getOriginalFilename();
            // 文件路径
            String path = DateUtils.dateToStr(new Date(), DateUtils.yyyyMMddNo) + File.separator + originalName;
            String pathAll = getRootPath() + path;
            File targetFile = new File(pathAll);
            if (!targetFile.getParentFile().exists()) {
                targetFile.getParentFile().mkdirs();
            }
            try {
                file.transferTo(targetFile);
                SysFile sysFile = new SysFile();
                sysFile.setOriginalName(originalName);
                sysFile.setFileSize(file.getSize());
                sysFile.setUrl(path);
                SysFile result = sysFileService.save(sysFile).getData();
                return ResultModel.success(result);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return ResultModel.fail();
        }
    }

    @OperationLog(title = "系统文件-根据url获取图片", businessType = BusinessType.SELECT)
    @ApiOperation(value = "系统文件-根据url获取图片")
    @GetMapping("/getByUrl")
    @ResponseBody
    public String getByUrl(@RequestParam(name = "url") String url,
                         HttpServletResponse response) {
        String pathAll = getRootPath() + url;

        ServletOutputStream outputStream = null;
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(new File(pathAll));
            outputStream = response.getOutputStream();
            //读取文件流
            int len = 0;
            byte[] buffer = new byte[1024 * 8];
            while ((len = fileInputStream.read(buffer)) != -1){
                outputStream.write(buffer, 0, len);
            }
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

}
