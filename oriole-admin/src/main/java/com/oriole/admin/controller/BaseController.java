package com.oriole.admin.controller;

import com.oriole.common.constant.Constants;
import com.oriole.entity.sys.SysMenu;
import com.oriole.entity.vo.LayuiTreeSysMenu;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/15 21:44
 * Description:
 */
public class BaseController {

    protected static List<LayuiTreeSysMenu> getLayuiTreeSysMenus(List<SysMenu> sysMenus) {
        List<LayuiTreeSysMenu> layuiTreeSysMenus = new ArrayList<>();

        List<SysMenu> firstSysMenus = sysMenus.stream()
                .filter(sysMenu -> Constants.GOD.equals(sysMenu.getParentId()))
                .collect(Collectors.toList());
        List<SysMenu> secondSysMenus = sysMenus.stream()
                .filter(sysMenu -> !Constants.GOD.equals(sysMenu.getParentId()))
                .collect(Collectors.toList());
        firstSysMenus.forEach(firstSysMenu -> {
            LayuiTreeSysMenu layuiTreeSysMenu = new LayuiTreeSysMenu();
            layuiTreeSysMenu.setTitle(firstSysMenu.getName());
            layuiTreeSysMenu.setId(firstSysMenu.getId());
            layuiTreeSysMenu.setField("name");
            ArrayList<LayuiTreeSysMenu> children = new ArrayList<>();
            for (SysMenu secondSysMenu : secondSysMenus) {
                if (secondSysMenu.getParentId().equals(firstSysMenu.getId())) {
                    LayuiTreeSysMenu secondLayuiTreeSysMenu = new LayuiTreeSysMenu();
                    secondLayuiTreeSysMenu.setTitle(secondSysMenu.getName());
                    secondLayuiTreeSysMenu.setId(secondSysMenu.getId());
                    secondLayuiTreeSysMenu.setField("name");
                    children.add(secondLayuiTreeSysMenu);
                }
            }
            layuiTreeSysMenu.setChildren(children);

            layuiTreeSysMenus.add(layuiTreeSysMenu);
        });
        if (layuiTreeSysMenus.size() > 0) {
            layuiTreeSysMenus.get(0).setSpread(Boolean.TRUE);
        }
        return layuiTreeSysMenus;
    }

    protected String getRootPath() {
        return System.getProperty("user.home") + File.separator + "oriole" + File.separator;
    }
}
