package com.oriole.admin.controller;

import com.oriole.admin.feign.ISysDictionaryDataService;
import com.oriole.admin.feign.ISysDictionaryTypeService;
import com.oriole.admin.feign.ISysNoticeService;
import com.oriole.common.annotation.OperationLog;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.common.enums.BusinessType;
import com.oriole.entity.sys.SysDictionaryData;
import com.oriole.entity.sys.SysDictionaryType;
import com.oriole.entity.sys.SysNotice;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/12/13 13:09
 * Description:
 */
@Api(tags = {"系统通知Controller"})
@Controller
@RequestMapping("/sysNotice")
public class SysNoticeController {

    @Resource
    private ISysNoticeService sysNoticeService;

    @Resource
    private ISysDictionaryTypeService sysDictionaryTypeService;

    @Resource
    private ISysDictionaryDataService sysDictionaryDataService;

    @OperationLog(title = "系统通知-列表页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统通知-列表页面")
    @GetMapping("/index")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("sysNotice/index");
        return modelAndView;
    }

    @OperationLog(title = "系统通知-获取列表分页", businessType = BusinessType.SELECT)
    @ApiOperation(value = "系统通知-获取列表分页")
    @ResponseBody
    @GetMapping("/findPage")
    public ResultModel<Object> findPage(OriolePage oriolePage) {
        ResultModel<Object> resultModel = sysNoticeService.findPage(oriolePage);
        return ResultModel.success(PageableUtil.setResultPage(resultModel));
    }

    @OperationLog(title = "系统通知-新增页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统通知-新增页面")
    @GetMapping("/toSave")
    public ModelAndView toSave() {
        ModelAndView modelAndView = new ModelAndView("/sysNotice/toSave");
        SysDictionaryType sysDictionaryType = sysDictionaryTypeService.findByType("sys_notice_type").getData();
        ResultModel<List<SysDictionaryData>> resultModel = sysDictionaryDataService.findByTypeId(sysDictionaryType.getId());
        List<SysDictionaryData> sysDictionaryDatas = resultModel.getData();
        modelAndView.addObject("sysDictionaryDatas", sysDictionaryDatas);
        return modelAndView;
    }

    @OperationLog(title = "系统通知-保存", businessType = BusinessType.INSERT)
    @ApiOperation(value = "系统通知-保存")
    @ResponseBody
    @PostMapping("/save")
    public ResultModel<SysNotice> save(@Valid @RequestBody SysNotice sysNotice) {
        return sysNoticeService.save(sysNotice);
    }

    @OperationLog(title = "系统通知-查看页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统通知-查看页面")
    @GetMapping("/toDetail/{id}")
    public ModelAndView toDetail(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/sysNotice/toDetail");
        SysNotice sysNotice = sysNoticeService.findById(id).getData();
        modelAndView.addObject("sysNotice", sysNotice);
        SysDictionaryType sysDictionaryType = sysDictionaryTypeService.findByType("sys_notice_type").getData();
        ResultModel<List<SysDictionaryData>> resultModel = sysDictionaryDataService.findByTypeId(sysDictionaryType.getId());
        List<SysDictionaryData> sysDictionaryDatas = resultModel.getData();
        modelAndView.addObject("sysDictionaryDatas", sysDictionaryDatas);
        return modelAndView;
    }

    @OperationLog(title = "系统通知-编辑页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统通知-编辑页面")
    @GetMapping("/toUpdate/{id}")
    public ModelAndView toUpdate(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/sysNotice/toUpdate");
        SysNotice sysNotice = sysNoticeService.findById(id).getData();
        modelAndView.addObject("sysNotice", sysNotice);
        SysDictionaryType sysDictionaryType = sysDictionaryTypeService.findByType("sys_notice_type").getData();
        ResultModel<List<SysDictionaryData>> resultModel = sysDictionaryDataService.findByTypeId(sysDictionaryType.getId());
        List<SysDictionaryData> sysDictionaryDatas = resultModel.getData();
        modelAndView.addObject("sysDictionaryDatas", sysDictionaryDatas);
        return modelAndView;
    }

    @OperationLog(title = "系统通知-编辑", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "系统通知-编辑")
    @ResponseBody
    @PutMapping("/update")
    public ResultModel<SysNotice> update(@Valid @RequestBody SysNotice sysNotice) {
        return sysNoticeService.update(sysNotice);
    }

    @OperationLog(title = "系统通知-删除", businessType = BusinessType.DELETE)
    @ApiOperation(value = "系统通知-删除")
    @ResponseBody
    @DeleteMapping("/delete/{id}")
    public ResultModel<Object> delete(@PathVariable("id") Long id) {
        return sysNoticeService.deleteById(id);
    }

}
