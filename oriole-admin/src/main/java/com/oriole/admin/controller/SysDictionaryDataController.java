package com.oriole.admin.controller;

import com.oriole.admin.feign.ISysDictionaryDataService;
import com.oriole.admin.feign.ISysDictionaryTypeService;
import com.oriole.common.annotation.OperationLog;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.common.enums.BusinessType;
import com.oriole.entity.sys.SysDictionaryData;
import com.oriole.entity.sys.SysDictionaryType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/2 19:36
 * Description:
 */
@Api(tags = {"字典数据-SysDictionaryDataController"})
@Controller
@RequestMapping("/sysDictionaryData")
public class SysDictionaryDataController {

    @Resource
    private ISysDictionaryDataService sysDictionaryDataService;

    @Resource
    private ISysDictionaryTypeService sysDictionaryTypeService;

    @OperationLog(title = "字典数据-进入首页", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "字典数据-进入首页")
    @GetMapping("/index")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("/sysDictionaryData/index");
        return modelAndView;
    }

    @OperationLog(title = "字典数据-获取列表分页", businessType = BusinessType.SELECT)
    @ApiOperation(value = "字典数据-获取列表分页")
    @ResponseBody
    @GetMapping("/findPage")
    public ResultModel<Object> findPage(OriolePage oriolePage) {
        ResultModel<Object> resultModel = sysDictionaryDataService.findPage(oriolePage);
        return ResultModel.success(PageableUtil.setResultPage(resultModel));
    }

    @OperationLog(title = "字典数据-新增页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "字典数据-新增页面")
    @GetMapping("/toSave")
    public ModelAndView toSave() {
        ModelAndView modelAndView = new ModelAndView("/sysDictionaryData/toSave");
        List<SysDictionaryType> sysDictionaryTypes = sysDictionaryTypeService.findAll().getData();
        modelAndView.addObject("sysDictionaryTypes",sysDictionaryTypes);
        return modelAndView;
    }

    @OperationLog(title = "字典数据-新增", businessType = BusinessType.INSERT)
    @ApiOperation(value = "字典数据-新增")
    @PostMapping("/save")
    @ResponseBody
    public ResultModel<SysDictionaryData> save(@Valid @RequestBody SysDictionaryData sysDictionaryData) {
        return sysDictionaryDataService.save(sysDictionaryData);
    }

    @OperationLog(title = "字典数据-查看页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "字典数据-查看页面")
    @GetMapping("/toDetail/{id}")
    public ModelAndView toDetail(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/sysDictionaryData/toDetail");
        SysDictionaryData sysDictionaryData = sysDictionaryDataService.findById(id).getData();
        modelAndView.addObject("sysDictionaryData", sysDictionaryData);
        return modelAndView;
    }

    @OperationLog(title = "字典数据-编辑页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "字典数据-编辑页面")
    @GetMapping("/toUpdate/{id}")
    public ModelAndView toUpdate(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/sysDictionaryData/toUpdate");
        SysDictionaryData sysDictionaryData = sysDictionaryDataService.findById(id).getData();
        modelAndView.addObject("sysDictionaryData", sysDictionaryData);
        List<SysDictionaryType> sysDictionaryTypes = sysDictionaryTypeService.findAll().getData();
        modelAndView.addObject("sysDictionaryTypes",sysDictionaryTypes);
        return modelAndView;
    }

    @OperationLog(title = "字典数据-编辑", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "字典数据-编辑")
    @ResponseBody
    @PutMapping("/update")
    public ResultModel<SysDictionaryData> update(@Valid @RequestBody SysDictionaryData sysDictionaryData) {
        return sysDictionaryDataService.update(sysDictionaryData);
    }

    @OperationLog(title = "字典数据-删除", businessType = BusinessType.DELETE)
    @ApiOperation(value = "字典数据-删除")
    @ResponseBody
    @DeleteMapping("/delete/{id}")
    public ResultModel<Object> delete(@PathVariable("id") Long id) {
        return sysDictionaryDataService.deleteById(id);
    }

}
