package com.oriole.admin.controller;

import com.oriole.admin.feign.ISysConfigService;
import com.oriole.common.annotation.OperationLog;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.enums.BusinessType;
import com.oriole.entity.vo.SysConfigVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/4 23:24
 * Description:
 */
@Api(tags = {"系统配置Controller"})
@Controller
@RequestMapping("/sysConfig")
public class SysConfigController {

    @Resource
    private ISysConfigService sysConfigService;

    @OperationLog(title = "系统配置", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统配置-首页")
    @GetMapping("/index")
    public ModelAndView index() {
        // 系统基本配置
        SysConfigVo sysConfigVo = sysConfigService.findSysConfigVo().getData();

        ModelAndView modelAndView = new ModelAndView("/sysConfig/index");
        modelAndView.addObject("sysConfigVo", sysConfigVo);

        return modelAndView;
    }

    @OperationLog(title = "系统配置-编辑", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "系统配置-编辑")
    @PutMapping("/update")
    @ResponseBody
    public ResultModel<SysConfigVo> update(@RequestBody SysConfigVo sysConfigVo) {
        return sysConfigService.update(sysConfigVo);
    }

}
