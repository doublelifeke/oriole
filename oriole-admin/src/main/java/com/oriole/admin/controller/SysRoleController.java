package com.oriole.admin.controller;

import com.oriole.admin.feign.ISysMenuService;
import com.oriole.admin.feign.ISysRoleService;
import com.oriole.common.annotation.OperationLog;
import com.oriole.common.constant.Constants;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.common.enums.BusinessType;
import com.oriole.entity.sys.SysMenu;
import com.oriole.entity.sys.SysRole;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/11 22:40
 * Description:
 */
@Api(tags = {"系统角色-SysRoleController"})
@Controller
@RequestMapping("/sysRole")
public class SysRoleController {

    @Resource
    private ISysRoleService sysRoleService;

    @Resource
    private ISysMenuService sysMenuService;

    @OperationLog(title = "系统角色-首页", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统角色-首页")
    @GetMapping("/index")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("/sysRole/index");
        return modelAndView;
    }

    @OperationLog(title = "系统角色-获取系统角色列表", businessType = BusinessType.SELECT)
    @ApiOperation(value = "系统角色-获取系统角色列表")
    @ResponseBody
    @GetMapping("/findAll")
    @Secured("ROLE_SUPER")
    public ResultModel<List<SysRole>> findAll() {
        return sysRoleService.findAll();
    }

    @OperationLog(title = "系统角色-获取系统角色列表分页", businessType = BusinessType.SELECT)
    @ApiOperation(value = "系统角色-获取系统角色列表分页")
    @ResponseBody
    @GetMapping("/findPage")
    @Secured("ROLE_SUPER")
    public ResultModel<Object> findPage(OriolePage oriolePage) {
        ResultModel<Object> resultModel = sysRoleService.findPage(oriolePage);
        return ResultModel.success(PageableUtil.setResultPage(resultModel));
    }

    @OperationLog(title = "系统角色-新增页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统角色-新增页面")
    @GetMapping("/toSave")
    public ModelAndView toSave() {
        ModelAndView modelAndView = new ModelAndView("/sysRole/toSave");
        return modelAndView;
    }

    @OperationLog(title = "系统角色-保存角色", businessType = BusinessType.INSERT)
    @ApiOperation(value = "系统角色-保存角色")
    @ResponseBody
    @PostMapping("/save")
    @Secured("ROLE_SUPER")
    public ResultModel<SysRole> save(@Valid @RequestBody SysRole sysRole) {
        return sysRoleService.save(sysRole);
    }

    @OperationLog(title = "系统角色-查看页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统角色-查看页面")
    @GetMapping("/toDetail/{id}")
    public ModelAndView toDetail(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/sysRole/toDetail");
        SysRole sysRole = sysRoleService.findById(id).getData();
        modelAndView.addObject("sysRole", sysRole);
        return modelAndView;
    }

    @OperationLog(title = "系统角色-编辑页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统角色-编辑页面")
    @GetMapping("/toUpdate/{id}")
    public ModelAndView toUpdate(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/sysRole/toUpdate");
        SysRole sysRole = sysRoleService.findById(id).getData();
        modelAndView.addObject("sysRole", sysRole);
        return modelAndView;
    }

    @OperationLog(title = "系统角色-修改角色", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "系统角色-修改角色")
    @ResponseBody
    @PutMapping("/update")
    @Secured("ROLE_SUPER")
    public ResultModel<SysRole> update(@Valid @RequestBody SysRole sysRole) {
        return sysRoleService.update(sysRole);
    }

    @OperationLog(title = "系统角色-删除角色", businessType = BusinessType.DELETE)
    @ApiOperation(value = "系统角色-删除角色")
    @ResponseBody
    @DeleteMapping("/delete/{id}")
    @Secured("ROLE_SUPER")
    public ResultModel<Object> delete(@PathVariable("id") Long id) {
        return sysRoleService.deleteById(id);
    }

    @OperationLog(title = "系统角色-角色设置菜单-页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统角色-角色设置菜单-页面")
    @GetMapping("/toSetSysMenu/{id}")
    public ModelAndView toSetSysMenu(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/sysRole/toSetSysMenu");
        modelAndView.addObject("sysRoleId", id);
        List<Long> checkedIds = sysMenuService.findBySysRoleId(id).getData()
                .stream().filter(sysMenu -> !sysMenu.getParentId().equals(Constants.GOD))
                .map(SysMenu::getId).collect(Collectors.toList());
        modelAndView.addObject("checkedIds", checkedIds);
        return modelAndView;
    }

    @OperationLog(title = "系统角色-角色设置菜单", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "系统角色-角色设置菜单")
    @ResponseBody
    @PutMapping("/setSysMenu")
    public ResultModel<SysRole> setSysMenu(@RequestParam String sysMenuIds,
                                           @RequestParam Long sysRoleId) {
        return sysRoleService.setSysMenu(sysMenuIds, sysRoleId);
    }

}
