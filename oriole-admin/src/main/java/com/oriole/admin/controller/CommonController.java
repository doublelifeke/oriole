package com.oriole.admin.controller;

import com.oriole.common.annotation.OperationLog;
import com.oriole.common.enums.BusinessType;
import com.oriole.common.util.HttpClientUtil;
import com.oriole.common.util.JacksonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/8/17 23:29
 * description:
 */
@Api(tags = {"系统公用Controller"})
@Controller
public class CommonController {

    @Value("${server.port}")
    private String port;

    @OperationLog(title = "404页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "404页面")
    @GetMapping("/404")
    public String error401() {
        return "404";
    }

    @OperationLog(title = "500页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "500页面")
    @GetMapping("/500")
    public String error500() {
        return "500";
    }

    @OperationLog(title = "系统环境页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "系统环境页面")
    @GetMapping("monitor")
    public String monitor() {
        return "/common/monitor";
    }

    @OperationLog(title = "跳转实时日志", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "跳转实时日志")
    @GetMapping("logging")
    public ModelAndView logging() {
        return new ModelAndView("/common/logging");
    }

    @OperationLog(title = "home页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "Oriole-home页面")
    @GetMapping("home")
    public ModelAndView home() {
        return new ModelAndView("/common/home");
    }

    @OperationLog(title = "actuator页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "actuator页面")
    @GetMapping("/oriole/actuator/index")
    public ModelAndView actuator() {
        String result = HttpClientUtil.sendGet("http://localhost:" + port + "/actuator");
        Map<String, Object> outerMap = JacksonUtil.json2Map(result);
        Map<String, Object> links = JacksonUtil.json2Map(JacksonUtil.pojo2Json(outerMap.get("_links")));
        ModelAndView modelAndView = new ModelAndView("/common/actuator");
        modelAndView.addObject("links", links);
        return modelAndView;
    }
}
