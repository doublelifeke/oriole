package com.oriole.admin.controller;

import com.oriole.admin.feign.ISysJobService;
import com.oriole.common.annotation.OperationLog;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.common.enums.BusinessType;
import com.oriole.entity.sys.SysJob;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/18 18:52
 * Description:
 */
@Api(tags = {"定时任务-SysJobController"})
@Controller
@RequestMapping("/sysJob")
public class SysJobController {

    @Resource
    private ISysJobService sysJobService;

    @OperationLog(title = "定时任务-进入首页", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "定时任务-进入首页")
    @GetMapping("/index")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("/sysJob/index");
        return modelAndView;
    }

    @OperationLog(title = "定时任务-获取任务列表分页", businessType = BusinessType.SELECT)
    @ApiOperation(value = "定时任务-获取任务列表分页")
    @ResponseBody
    @GetMapping("/findPage")
    public ResultModel<Object> findPage(OriolePage oriolePage) {
        ResultModel<Object> resultModel = sysJobService.findPage(oriolePage);
        return ResultModel.success(PageableUtil.setResultPage(resultModel));
    }

    @OperationLog(title = "定时任务-任务调度状态修改", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "定时任务-任务调度状态修改")
    @PutMapping("/changeStatus")
    @ResponseBody
    public ResultModel<SysJob> changeStatus(@RequestBody SysJob sysJob) {
        return sysJobService.changeStatus(sysJob);
    }

    @OperationLog(title = "定时任务-根据ID删除定时任务", businessType = BusinessType.DELETE)
    @ApiOperation(value = "定时任务-根据ID删除定时任务")
    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public ResultModel<Object> deleteById(@PathVariable("id") Long id) {
        return sysJobService.deleteById(id);
    }

    @OperationLog(title = "定时任务-新增页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "定时任务-新增页面")
    @GetMapping("/toSave")
    public ModelAndView toSave() {
        ModelAndView modelAndView = new ModelAndView("/sysJob/toSave");
        return modelAndView;
    }

    @OperationLog(title = "定时任务-新增", businessType = BusinessType.INSERT)
    @ApiOperation(value = "定时任务-新增")
    @PostMapping("/save")
    @ResponseBody
    public ResultModel<SysJob> save(@Valid @RequestBody SysJob sysJob) {
        return sysJobService.save(sysJob);
    }

    @OperationLog(title = "定时任务-编辑页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "定时任务-编辑页面")
    @GetMapping("/toUpdate/{id}")
    public ModelAndView toUpdate(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/sysJob/toUpdate");
        SysJob sysJob = sysJobService.findById(id).getData();
        modelAndView.addObject("sysJob", sysJob);
        return modelAndView;
    }

    @OperationLog(title = "定时任务-编辑", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "定时任务-编辑")
    @ResponseBody
    @PutMapping("/update")
    public ResultModel<SysJob> update(@Valid @RequestBody SysJob sysJob) {
        return sysJobService.update(sysJob);
    }

    @OperationLog(title = "定时任务-查看页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "定时任务-查看页面")
    @GetMapping("/toDetail/{id}")
    public ModelAndView toDetail(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/sysJob/toDetail");
        SysJob sysJob = sysJobService.findById(id).getData();
        modelAndView.addObject("sysJob", sysJob);
        return modelAndView;
    }

    @OperationLog(title = "定时任务-任务调度立即执行一次", businessType = BusinessType.OTHER)
    @ApiOperation(value = "定时任务-任务调度立即执行一次")
    @PostMapping("/run/{id}")
    @ResponseBody
    public ResultModel<Object> run(@PathVariable Long id) {
        return sysJobService.run(id);
    }

}
