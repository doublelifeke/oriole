package com.oriole.admin.controller;

import com.oriole.admin.feign.ISysDictionaryTypeService;
import com.oriole.common.annotation.OperationLog;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.common.enums.BusinessType;
import com.oriole.entity.sys.SysDictionaryType;
import com.oriole.entity.sys.SysJob;
import com.oriole.entity.sys.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/2 19:35
 * Description:
 */
@Api(tags = {"字典类型-SysDictionaryTypeController"})
@Controller
@RequestMapping("/sysDictionaryType")
public class SysDictionaryTypeController {

    @Resource
    private ISysDictionaryTypeService sysDictionaryTypeService;

    @OperationLog(title = "字典类型-进入首页", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "字典类型-进入首页")
    @GetMapping("/index")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("/sysDictionaryType/index");
        return modelAndView;
    }

    @OperationLog(title = "字典类型-获取列表分页", businessType = BusinessType.SELECT)
    @ApiOperation(value = "字典类型-获取列表分页")
    @ResponseBody
    @GetMapping("/findPage")
    public ResultModel<Object> findPage(OriolePage oriolePage) {
        ResultModel<Object> resultModel = sysDictionaryTypeService.findPage(oriolePage);
        return ResultModel.success(PageableUtil.setResultPage(resultModel));
    }

    @OperationLog(title = "字典类型-新增页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "字典类型-新增页面")
    @GetMapping("/toSave")
    public ModelAndView toSave() {
        ModelAndView modelAndView = new ModelAndView("/sysDictionaryType/toSave");
        return modelAndView;
    }

    @OperationLog(title = "字典类型-新增", businessType = BusinessType.INSERT)
    @ApiOperation(value = "字典类型-新增")
    @PostMapping("/save")
    @ResponseBody
    public ResultModel<SysDictionaryType> save(@Valid @RequestBody SysDictionaryType sysDictionaryType) {
        return sysDictionaryTypeService.save(sysDictionaryType);
    }

    @OperationLog(title = "字典类型-查看页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "字典类型-查看页面")
    @GetMapping("/toDetail/{id}")
    public ModelAndView toDetail(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/sysDictionaryType/toDetail");
        SysDictionaryType sysDictionaryType = sysDictionaryTypeService.findById(id).getData();
        modelAndView.addObject("sysDictionaryType", sysDictionaryType);
        return modelAndView;
    }

    @OperationLog(title = "字典类型-编辑页面", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "字典类型-编辑页面")
    @GetMapping("/toUpdate/{id}")
    public ModelAndView toUpdate(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/sysDictionaryType/toUpdate");
        SysDictionaryType sysDictionaryType = sysDictionaryTypeService.findById(id).getData();
        modelAndView.addObject("sysDictionaryType", sysDictionaryType);
        return modelAndView;
    }

    @OperationLog(title = "字典类型-编辑", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "字典类型-编辑")
    @ResponseBody
    @PutMapping("/update")
    public ResultModel<SysDictionaryType> update(@Valid @RequestBody SysDictionaryType sysDictionaryType) {
        return sysDictionaryTypeService.update(sysDictionaryType);
    }

    @OperationLog(title = "字典类型-删除", businessType = BusinessType.DELETE)
    @ApiOperation(value = "字典类型-删除")
    @ResponseBody
    @DeleteMapping("/delete/{id}")
    public ResultModel<Object> delete(@PathVariable("id") Long id) {
        return sysDictionaryTypeService.deleteById(id);
    }

}
