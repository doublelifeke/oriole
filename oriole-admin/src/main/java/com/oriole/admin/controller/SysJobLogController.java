package com.oriole.admin.controller;

import com.oriole.admin.feign.ISysJobLogService;
import com.oriole.common.annotation.OperationLog;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.common.enums.BusinessType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/1 21:03
 * Description:
 */
@Api(tags = {"系统定时任务日志-SysJobLogController"})
@Controller
@RequestMapping("/sysJobLog")
public class SysJobLogController {

    @Resource
    private ISysJobLogService sysJobLogService;

    @OperationLog(title = "定时任务日志-进入首页", businessType = BusinessType.TO_PAGE)
    @ApiOperation(value = "定时任务日志-进入首页")
    @GetMapping("/index")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("/sysJobLog/index");
        return modelAndView;
    }

    @OperationLog(title = "定时任务日志-获取任务日志列表分页", businessType = BusinessType.SELECT)
    @ApiOperation(value = "定时任务日志-获取任务日志列表分页")
    @ResponseBody
    @GetMapping("/findPage")
    public ResultModel<Object> findPage(OriolePage oriolePage) {
        ResultModel<Object> resultModel = sysJobLogService.findPage(oriolePage);
        return ResultModel.success(PageableUtil.setResultPage(resultModel));
    }

    @OperationLog(title = "定时任务日志-根据ID删除定时任务", businessType = BusinessType.DELETE)
    @ApiOperation(value = "定时任务日志-根据ID删除定时任务")
    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public ResultModel<Object> deleteById(@PathVariable("id") Long id) {
        return sysJobLogService.deleteById(id);
    }

}
