package com.oriole.admin.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2019/3/28
 * Time: 13:00
 * description:
 *  文档地址：http://localhost:9001/swagger-ui.html
 *  knife4j: http://localhost:9001/doc.html
 * <p>
 * Swagger2 基本使用：
 * 常用注解：
 * - @Api()用于类；
 * 表示标识这个类是swagger的资源
 * - @ApiOperation()用于方法；
 * 表示一个http请求的操作
 * - @ApiParam()用于方法，参数，字段说明；
 * 表示对参数的添加元数据（说明或是否必填等）
 * - @ApiModel()用于类
 * 表示对类进行说明，用于参数用实体类接收
 * - @ApiModelProperty()用于方法，字段
 * 表示对model属性的说明或者数据操作更改
 * - @ApiIgnore()用于类，方法，方法参数
 * 表示这个方法或者类被忽略
 * - @ApiImplicitParam() 用于方法
 * 表示单独的请求参数
 * - @ApiImplicitParams() 用于方法，包含多个 @ApiImplicitParam
 */
@EnableSwagger2
@EnableKnife4j
@Configuration
public class SwaggerConfig {

    /**
     * 是否开启swagger，正式环境一般是需要关闭的，
     * 可根据springboot的多环境配置进行设置
     */
    @Value(value = "${swagger.enabled}")
    Boolean swaggerEnabled;

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                // 是否开启
                .enable(swaggerEnabled)
                .select()
                // 扫描的路径包
                .apis(RequestHandlerSelectors.basePackage("com.oriole.admin.controller"))
                // 指定路径处理PathSelectors.any()代表所有的路径
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("oriole-admin")
                .description("ORIOLE接口文档")
                .termsOfServiceUrl("https://gitee.com/doublelifeke/oriole")
                .version("1.0")
                .build();
    }
}
