package com.oriole.admin.config;

import feign.Request;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/3/11
 * Time: 14:55
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Bean
    Request.Options feignOptions() {
        return new Request.Options();
    }

}
