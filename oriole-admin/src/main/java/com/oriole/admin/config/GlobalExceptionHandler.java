package com.oriole.admin.config;

import com.oriole.common.constant.ResultModel;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/9/12 2:23
 * description: GlobalException 只处理经过Controller
 */
@RestControllerAdvice
@ConditionalOnProperty(prefix = "oriole", name = "global-exception", havingValue = "true")
public class GlobalExceptionHandler {
    @ExceptionHandler(value = Exception.class)
    public ResultModel<Object> handler(Exception e) {
        return new ResultModel<>(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
    }
}
