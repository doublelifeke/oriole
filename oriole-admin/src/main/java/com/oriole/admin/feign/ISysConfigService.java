package com.oriole.admin.feign;

import com.oriole.admin.feign.fallback.SysConfigServiceFallBack;
import com.oriole.common.constant.ResultModel;
import com.oriole.entity.sys.SysConfig;
import com.oriole.entity.vo.SysConfigVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/4 23:25
 * Description:
 */
@FeignClient(name = "oriole-api", fallback = SysConfigServiceFallBack.class)
public interface ISysConfigService {

    /**
     * 系统基本配置信息
     *
     * @return 结果
     */
    @GetMapping("/api/sysConfig/findSysConfigVo")
    ResultModel<SysConfigVo> findSysConfigVo();

    /**
     * 根据key获取配置信息
     *
     * @param key key
     * @return 结果
     */
    @GetMapping("/api/sysConfig/findByConfigKey")
    ResultModel<SysConfig> findByConfigKey(@RequestParam("key") String key);

    @PutMapping("/api/sysConfig/update")
    ResultModel<SysConfigVo> update(@RequestBody SysConfigVo sysConfigVo);
}
