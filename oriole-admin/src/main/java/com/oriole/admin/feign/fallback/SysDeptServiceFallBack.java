package com.oriole.admin.feign.fallback;

import com.oriole.admin.feign.ISysDeptService;
import com.oriole.common.constant.Constants;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.constant.ResultStatus;
import com.oriole.common.controller.OriolePage;
import com.oriole.entity.sys.SysDept;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/14 18:05
 * Description:
 */
@Component
public class SysDeptServiceFallBack implements ISysDeptService {

    @Override
    public ResultModel<List<SysDept>> findAll() {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<Object> findPage(OriolePage oriolePage) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysDept> save(SysDept sysDept) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysDept> findById(Long id) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysDept> update(SysDept sysDept) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<Object> deleteById(Long id) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }
}
