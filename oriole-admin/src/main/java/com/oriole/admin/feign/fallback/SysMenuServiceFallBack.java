package com.oriole.admin.feign.fallback;

import com.oriole.admin.feign.ISysMenuService;
import com.oriole.common.constant.Constants;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.constant.ResultStatus;
import com.oriole.entity.sys.SysMenu;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/14 18:09
 * Description:
 */
@Component
public class SysMenuServiceFallBack implements ISysMenuService {

    /**
     * 根据用户Id获取菜单
     *
     * @param userId 用户ID
     * @return 菜单列表
     */
    @Override
    public ResultModel<List<SysMenu>> findByUserId(Long userId) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    /**
     * 根据用户Id获取菜单
     *
     * @param userId 用户ID
     * @return 快捷菜单列表
     */
    @Override
    public ResultModel<List<SysMenu>> findShortcutByUserId(Long userId) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    /**
     * 获取所有菜单列表
     *
     * @return 菜单列表
     */
    @Override
    public ResultModel<List<SysMenu>> findAll() {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysMenu> save(SysMenu sysMenu) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysMenu> findById(Long id) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysMenu> update(SysMenu sysMenu) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<Object> deleteById(Long id) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    /**
     * 根据角色ID查询菜单
     *
     * @param sysRoleId
     * @return
     */
    @Override
    public ResultModel<List<SysMenu>> findBySysRoleId(Long sysRoleId) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }
}
