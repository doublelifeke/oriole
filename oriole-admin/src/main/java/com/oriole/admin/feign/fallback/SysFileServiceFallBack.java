package com.oriole.admin.feign.fallback;

import com.oriole.admin.feign.ISysFileService;
import com.oriole.common.constant.Constants;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.constant.ResultStatus;
import com.oriole.entity.sys.SysFile;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/14 18:08
 * Description:
 */
@Component
public class SysFileServiceFallBack implements ISysFileService {

    @Override
    public ResultModel<SysFile> save(SysFile sysFile) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }
}
