package com.oriole.admin.feign;

import com.oriole.admin.feign.fallback.SysRoleServiceFallBack;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.entity.sys.SysRole;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/11 22:43
 * Description:
 */
@FeignClient(name = "oriole-api", fallback = SysRoleServiceFallBack.class)
public interface ISysRoleService {

    @GetMapping("/api/sysRole/findAll")
    ResultModel<List<SysRole>> findAll();

    @GetMapping("/api/sysRole/findPage")
    ResultModel<Object> findPage(@RequestBody OriolePage oriolePage);

    @PostMapping("/api/sysRole/save")
    ResultModel<SysRole> save(@RequestBody SysRole sysRole);

    @GetMapping("/api/sysRole/find/{id}")
    ResultModel<SysRole> findById(@PathVariable("id") Long id);

    @PutMapping("/api/sysRole/update")
    ResultModel<SysRole> update(@RequestBody SysRole sysRole);

    @DeleteMapping("/api/sysRole/delete/{id}")
    ResultModel<Object> deleteById(@PathVariable("id") Long id);

    /**
     * 角色分配菜单
     *
     * @param sysMenuIds 菜单ID 字符串 ,分割
     * @param sysRoleId  角色ID
     * @return ‘结果
     */
    @PutMapping("/api/sysRole/setSysMenu")
    ResultModel<SysRole> setSysMenu(@RequestParam String sysMenuIds, @RequestParam Long sysRoleId);

    /**
     * 根据用户ID查询拥有的角色
     *
     * @param sysUserId 用户ID
     * @return 结果
     */
    @GetMapping("/api/sysRole/findBySysUserId")
    ResultModel<List<SysRole>> findBySysUserId(@RequestParam("sysUserId") Long sysUserId);
}
