package com.oriole.admin.feign.fallback;

import com.oriole.admin.feign.ISysUserService;
import com.oriole.common.constant.Constants;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.constant.ResultStatus;
import com.oriole.common.controller.OriolePage;
import com.oriole.entity.sys.SysUser;
import com.oriole.entity.vo.SysUserVo;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/14 17:23
 * Description:
 */
@Component
public class SysUserServiceFallBack implements ISysUserService {

    @Override
    public ResultModel<SysUser> save(SysUser sysUser) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysUser> update(SysUser sysUser) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysUser> updateProfile(SysUser sysUser) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<Object> deleteById(Long id) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<Object> deleteBatchByIds(List<Long> ids) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<List<SysUser>> findAll() {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<Object> findPage(OriolePage oriolePage) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysUser> findByUsername(String username) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysUser> findById(Long id) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    /**
     * 查询用户基本信息
     *
     * @param id 主键ID
     * @return 用户基本信息
     */
    @Override
    public ResultModel<SysUserVo> findSysUserVoById(Long id) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    /**
     * 用户设置角色
     *
     * @param sysRoleIds 角色ID 字符串 ,分割
     * @param sysUserId  用户ID
     * @return 结果
     */
    @Override
    public ResultModel<SysUser> setSysRole(String sysRoleIds, Long sysUserId) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }
}
