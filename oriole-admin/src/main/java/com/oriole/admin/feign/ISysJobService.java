package com.oriole.admin.feign;

import com.oriole.admin.feign.fallback.SysJobServiceFallBack;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.entity.sys.SysJob;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/18 18:53
 * Description:
 */
@FeignClient(name = "oriole-api", fallback = SysJobServiceFallBack.class)
public interface ISysJobService {

    @GetMapping("/api/sysJob/findPage")
    ResultModel<Object> findPage(@RequestBody OriolePage oriolePage);

    /**
     * 任务调度状态修改
     *
     * @param sysJob 任务
     * @return 结果
     */
    @PutMapping("/api/sysJob/changeStatus")
    ResultModel<SysJob> changeStatus(@RequestBody SysJob sysJob);

    /**
     * 根据ID删除定时任务
     *
     * @param id 任务ID
     * @return 结果
     */
    @DeleteMapping("/api/sysJob/delete/{id}")
    ResultModel<Object> deleteById(@PathVariable("id") Long id);

    @PostMapping("/api/sysJob/save")
    ResultModel<SysJob> save(@RequestBody SysJob sysJob);

    @GetMapping("/api/sysJob/find/{id}")
    ResultModel<SysJob> findById(@PathVariable("id") Long id);

    @PutMapping("/api/sysJob/update")
    ResultModel<SysJob> update(@RequestBody SysJob sysJob);

    @PostMapping("/api/sysJob/run/{id}")
    ResultModel<Object> run(@PathVariable("id") Long id);
}
