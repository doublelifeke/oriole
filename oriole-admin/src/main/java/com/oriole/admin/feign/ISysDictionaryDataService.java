package com.oriole.admin.feign;

import com.oriole.admin.feign.fallback.SysDictionaryDataServiceFallBack;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.entity.sys.SysDictionaryData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/2 19:37
 * Description:
 */
@FeignClient(name = "oriole-api", fallback = SysDictionaryDataServiceFallBack.class)
public interface ISysDictionaryDataService {

    @GetMapping("/api/sysDictionaryData/findPage")
    ResultModel<Object> findPage(@RequestBody OriolePage oriolePage);

    @PostMapping("/api/sysDictionaryData/save")
    ResultModel<SysDictionaryData> save(@RequestBody SysDictionaryData sysDictionaryData);

    @GetMapping("/api/sysDictionaryData/find/{id}")
    ResultModel<SysDictionaryData> findById(@PathVariable("id") Long id);

    @PutMapping("/api/sysDictionaryData/update")
    ResultModel<SysDictionaryData> update(@RequestBody SysDictionaryData sysDictionaryData);

    @DeleteMapping("/api/sysDictionaryData/delete/{id}")
    ResultModel<Object> deleteById(@PathVariable("id") Long id);

    @GetMapping("/api/sysDictionaryData/findByTypeId")
    ResultModel<List<SysDictionaryData>> findByTypeId(@RequestParam("typeId") Long typeId);
}
