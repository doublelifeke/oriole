package com.oriole.admin.feign.fallback;

import com.oriole.admin.feign.ISysRoleService;
import com.oriole.common.constant.Constants;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.constant.ResultStatus;
import com.oriole.common.controller.OriolePage;
import com.oriole.entity.sys.SysRole;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/14 18:09
 * Description:
 */
@Component
public class SysRoleServiceFallBack implements ISysRoleService {

    @Override
    public ResultModel<List<SysRole>> findAll() {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<Object> findPage(OriolePage oriolePage) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysRole> save(SysRole sysRole) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysRole> findById(Long id) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysRole> update(SysRole sysRole) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<Object> deleteById(Long id) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    /**
     * 角色分配菜单
     *
     * @param sysMenuIds 菜单ID 字符串 ,分割
     * @param sysRoleId  角色ID
     * @return ‘结果
     */
    @Override
    public ResultModel<SysRole> setSysMenu(String sysMenuIds, Long sysRoleId) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    /**
     * 根据用户ID查询拥有的角色
     *
     * @param sysUserId 用户ID
     * @return 结果
     */
    @Override
    public ResultModel<List<SysRole>> findBySysUserId(Long sysUserId) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }
}
