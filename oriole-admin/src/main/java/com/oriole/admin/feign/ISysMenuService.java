package com.oriole.admin.feign;

import com.oriole.admin.feign.fallback.SysMenuServiceFallBack;
import com.oriole.common.constant.ResultModel;
import com.oriole.entity.sys.SysMenu;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/8/22 15:02
 * description:
 */
@FeignClient(name = "oriole-api", fallback = SysMenuServiceFallBack.class)
public interface ISysMenuService {

    /**
     * 根据用户Id获取菜单
     *
     * @param userId 用户ID
     * @return 菜单列表
     */
    @GetMapping("/api/sysMenu/findByUserId")
    ResultModel<List<SysMenu>> findByUserId(@RequestParam("userId") Long userId);

    /**
     * 根据用户Id获取菜单
     *
     * @param userId 用户ID
     * @return 快捷菜单列表
     */
    @GetMapping("/api/sysMenu/findShortcutByUserId")
    ResultModel<List<SysMenu>> findShortcutByUserId(@RequestParam("userId") Long userId);

    /**
     * 获取所有菜单列表
     *
     * @return 菜单列表
     */
    @GetMapping("/api/sysMenu/findAll")
    ResultModel<List<SysMenu>> findAll();

    @PostMapping("/api/sysMenu/save")
    ResultModel<SysMenu> save(@RequestBody SysMenu sysMenu);

    @GetMapping("/api/sysMenu/find/{id}")
    ResultModel<SysMenu> findById(@PathVariable("id") Long id);

    @PutMapping("/api/sysMenu/update")
    ResultModel<SysMenu> update(@RequestBody SysMenu sysMenu);

    @DeleteMapping("/api/sysMenu/delete/{id}")
    ResultModel<Object> deleteById(@PathVariable("id") Long id);

    /**
     * 根据角色ID查询菜单
     *
     * @param sysRoleId
     * @return
     */
    @GetMapping("/api/sysMenu/findBySysRoleId")
    ResultModel<List<SysMenu>> findBySysRoleId(@RequestParam("sysRoleId") Long sysRoleId);
}
