package com.oriole.admin.feign;

import com.oriole.admin.feign.fallback.SysJobLogServiceFallBack;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/1 21:04
 * Description:
 */
@FeignClient(name = "oriole-api", fallback = SysJobLogServiceFallBack.class)
public interface ISysJobLogService {

    @GetMapping("/api/sysJobLog/findPage")
    ResultModel<Object> findPage(@RequestBody OriolePage oriolePage);

    @DeleteMapping("/api/sysJobLog/delete/{id}")
    ResultModel<Object> deleteById(@PathVariable("id") Long id);

}
