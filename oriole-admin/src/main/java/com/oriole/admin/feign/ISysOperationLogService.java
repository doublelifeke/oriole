package com.oriole.admin.feign;

import com.oriole.admin.feign.fallback.SysOperationLogServiceFallBack;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.entity.sys.SysOperationLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/8 17:35
 * Description:
 */
@FeignClient(name = "oriole-api", fallback = SysOperationLogServiceFallBack.class)
public interface ISysOperationLogService {

    @PostMapping("/api/sysOperationLog/save")
    ResultModel<SysOperationLog> save(@RequestBody SysOperationLog sysOperationLog);

    @GetMapping("/api/sysOperationLog/findPage")
    ResultModel<Object> findPage(@RequestBody OriolePage oriolePage);

    @DeleteMapping("/api/sysOperationLog/delete/{id}")
    ResultModel<Object> deleteById(@PathVariable("id") Long id);
}
