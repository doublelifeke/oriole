package com.oriole.admin.feign;

import com.oriole.admin.feign.fallback.SysUserServiceFallBack;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.entity.sys.SysUser;
import com.oriole.entity.vo.SysUserVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Email: hautxxxyzjk@163.com
 * Date: 2019-04-02
 * Time: 19:55
 * Description: Hystric大坑：接口写@RequestMapping("/api/sysUser")项目启动报错：
 * Caused by: java.lang.IllegalStateException: Ambiguous mapping found. Cannot map ""
 */
@FeignClient(name = "oriole-api", fallback = SysUserServiceFallBack.class)
public interface ISysUserService {

    @PostMapping("/api/sysUser/save")
    ResultModel<SysUser> save(@RequestBody SysUser sysUser);

    @PutMapping("/api/sysUser/update")
    ResultModel<SysUser> update(@RequestBody SysUser sysUser);

    @PutMapping("/api/sysUser/updateProfile")
    ResultModel<SysUser> updateProfile(@RequestBody SysUser sysUser);

    @DeleteMapping("/api/sysUser/delete/{id}")
    ResultModel<Object> deleteById(@PathVariable("id") Long id);

    @DeleteMapping("/api/sysUser/deleteBatchByIds")
    ResultModel<Object> deleteBatchByIds(List<Long> ids);

    @GetMapping("/api/sysUser/findAll")
    ResultModel<List<SysUser>> findAll();

    @GetMapping("/api/sysUser/findPage")
    ResultModel<Object> findPage(@RequestBody OriolePage oriolePage);

    @GetMapping("/api/sysUser/findByUsername")
    ResultModel<SysUser> findByUsername(@RequestParam("username") String username);

    @GetMapping("/api/sysUser/find/{id}")
    ResultModel<SysUser> findById(@PathVariable("id") Long id);

    /**
     * 查询用户基本信息
     *
     * @param id 主键ID
     * @return 用户基本信息
     */
    @GetMapping("/api/sysUser/findBasicInfo/{id}")
    ResultModel<SysUserVo> findSysUserVoById(@PathVariable("id") Long id);

    /**
     * 用户设置角色
     *
     * @param sysRoleIds 角色ID 字符串 ,分割
     * @param sysUserId  用户ID
     * @return 结果
     */
    @PutMapping("/api/sysUser/setSysRole")
    ResultModel<SysUser> setSysRole(@RequestParam String sysRoleIds, @RequestParam Long sysUserId);

}
