package com.oriole.admin.feign;

import com.oriole.admin.feign.fallback.SysFileServiceFallBack;
import com.oriole.common.constant.ResultModel;
import com.oriole.entity.sys.SysFile;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/31 14:07
 * Description:
 */
@FeignClient(name = "oriole-api", fallback = SysFileServiceFallBack.class)
public interface ISysFileService {

    @PostMapping("/api/sysFile/save")
    ResultModel<SysFile> save(@RequestBody SysFile sysFile);

}
