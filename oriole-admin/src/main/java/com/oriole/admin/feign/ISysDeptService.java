package com.oriole.admin.feign;

import com.oriole.admin.feign.fallback.SysDeptServiceFallBack;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.entity.sys.SysDept;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/8 20:44
 * Description:
 */
@FeignClient(name = "oriole-api", fallback = SysDeptServiceFallBack.class)
public interface ISysDeptService {

    @GetMapping("/api/sysDept/findAll")
    ResultModel<List<SysDept>> findAll();

    @GetMapping("/api/sysDept/findPage")
    ResultModel<Object> findPage(@RequestBody OriolePage oriolePage);

    @PostMapping("/api/sysDept/save")
    ResultModel<SysDept> save(@RequestBody SysDept sysDept);

    @GetMapping("/api/sysDept/find/{id}")
    ResultModel<SysDept> findById(@PathVariable("id") Long id);

    @PutMapping("/api/sysDept/update")
    ResultModel<SysDept> update(@RequestBody SysDept sysDept);

    @DeleteMapping("/api/sysDept/delete/{id}")
    ResultModel<Object> deleteById(@PathVariable("id") Long id);
}
