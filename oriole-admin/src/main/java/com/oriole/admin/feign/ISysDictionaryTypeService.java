package com.oriole.admin.feign;

import com.oriole.admin.feign.fallback.SysDictionaryTypeServiceFallBack;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.entity.sys.SysDictionaryType;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/2 19:36
 * Description:
 */
@FeignClient(name = "oriole-api", fallback = SysDictionaryTypeServiceFallBack.class)
public interface ISysDictionaryTypeService {

    @GetMapping("/api/sysDictionaryType/findPage")
    ResultModel<Object> findPage(@RequestBody OriolePage oriolePage);

    @PostMapping("/api/sysDictionaryType/save")
    ResultModel<SysDictionaryType> save(@RequestBody SysDictionaryType sysDictionaryType);

    @GetMapping("/api/sysDictionaryType/find/{id}")
    ResultModel<SysDictionaryType> findById(@PathVariable("id") Long id);

    @PutMapping("/api/sysDictionaryType/update")
    ResultModel<SysDictionaryType> update(@RequestBody SysDictionaryType sysDictionaryType);

    @DeleteMapping("/api/sysDictionaryType/delete/{id}")
    ResultModel<Object> deleteById(@PathVariable("id") Long id);

    @GetMapping("/api/sysDictionaryType/findAll")
    ResultModel<List<SysDictionaryType>> findAll();

    @GetMapping("/api/sysDictionaryType/findByType")
    ResultModel<SysDictionaryType> findByType(@RequestParam("type") String type);

}
