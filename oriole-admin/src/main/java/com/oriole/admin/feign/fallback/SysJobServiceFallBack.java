package com.oriole.admin.feign.fallback;

import com.oriole.admin.feign.ISysJobService;
import com.oriole.common.constant.Constants;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.constant.ResultStatus;
import com.oriole.common.controller.OriolePage;
import com.oriole.entity.sys.SysJob;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/14 18:08
 * Description:
 */
@Component
public class SysJobServiceFallBack implements ISysJobService {

    @Override
    public ResultModel<Object> findPage(OriolePage oriolePage) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    /**
     * 任务调度状态修改
     *
     * @param sysJob 任务
     * @return 结果
     */
    @Override
    public ResultModel<SysJob> changeStatus(SysJob sysJob) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    /**
     * 根据ID删除定时任务
     *
     * @param id 任务ID
     * @return 结果
     */
    @Override
    public ResultModel<Object> deleteById(Long id) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysJob> save(SysJob sysJob) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysJob> findById(Long id) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysJob> update(SysJob sysJob) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<Object> run(Long id) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }
}
