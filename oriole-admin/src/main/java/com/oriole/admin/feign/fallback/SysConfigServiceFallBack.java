package com.oriole.admin.feign.fallback;

import com.oriole.admin.feign.ISysConfigService;
import com.oriole.common.constant.Constants;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.constant.ResultStatus;
import com.oriole.entity.sys.SysConfig;
import com.oriole.entity.vo.SysConfigVo;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/14 18:04
 * Description:
 */
@Component
public class SysConfigServiceFallBack implements ISysConfigService {

    /**
     * 系统基本配置信息
     *
     * @return 结果
     */
    @Override
    public ResultModel<SysConfigVo> findSysConfigVo() {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    /**
     * 根据key获取配置信息
     *
     * @param key key
     * @return 结果
     */
    @Override
    public ResultModel<SysConfig> findByConfigKey(String key) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }

    @Override
    public ResultModel<SysConfigVo> update(SysConfigVo sysConfigVo) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), Constants.REQUEST_TIME_OUT);
    }
}
