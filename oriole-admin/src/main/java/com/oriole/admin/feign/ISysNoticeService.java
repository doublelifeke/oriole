package com.oriole.admin.feign;

import com.oriole.admin.feign.fallback.SysNoticeServiceFallBack;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.entity.sys.SysDictionaryData;
import com.oriole.entity.sys.SysNotice;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/12/13 13:39
 * Description:
 */
@FeignClient(name = "oriole-api", fallback = SysNoticeServiceFallBack.class)
public interface ISysNoticeService {

    @GetMapping("/api/sysNotice/findPage")
    ResultModel<Object> findPage(@RequestBody OriolePage oriolePage);

    @PostMapping("/api/sysNotice/save")
    ResultModel<SysNotice> save(@RequestBody SysNotice sysNotice);

    @GetMapping("/api/sysNotice/find/{id}")
    ResultModel<SysNotice> findById(@PathVariable("id") Long id);

    @PutMapping("/api/sysNotice/update")
    ResultModel<SysNotice> update(SysNotice sysNotice);

    @DeleteMapping("/api/sysNotice/delete/{id}")
    ResultModel<Object> deleteById(@PathVariable("id") Long id);
}
