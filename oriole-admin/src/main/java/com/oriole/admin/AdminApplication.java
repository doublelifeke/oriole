package com.oriole.admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import java.net.InetAddress;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2019/3/21
 * Time: 11:35
 * 地址栏中：http://localhost:9002/hystrix 在url输入 http://localhost:9002/actuator/hystrix.stream
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
@EnableFeignClients // @EnableEurekaClient
@EnableHystrix
@EnableHystrixDashboard //开启服务监控
public class AdminApplication {

    private static final Logger log = LoggerFactory.getLogger(AdminApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${server.port}")
    private String port;

    @Bean
    public ApplicationRunner applicationRunner() {
        return applicationArguments -> {
            log.debug("\n////////////////////////////////////////////////////////////////////\n" +
                    "//                                                                //\n" +
                    "//              " + applicationName + " launched successfully.               //\n" +
                    "//              " + "http://" + InetAddress.getLocalHost().getHostAddress() + ":" + port + "                        //\n" +
                    "//                                                                //\n" +
                    "////////////////////////////////////////////////////////////////////");
        };
    }

}
