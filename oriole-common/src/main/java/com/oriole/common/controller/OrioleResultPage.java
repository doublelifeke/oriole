package com.oriole.common.controller;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/9/13 21:31
 * description:
 */
public class OrioleResultPage<T> extends OriolePage {

    private T content;

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }
}
