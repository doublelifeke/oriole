package com.oriole.common.constant;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/3/10 20:21
 * description: 常量定义
 */
public class Constants {

    /**
     * 造物主
     */
    public static final Long GOD = 0L;

    public static final String JWT_SIGNING_KEY = "SigningKey";

    public static final String HEADER_STRING = "Authorization";

    public static final String TOKEN_PREFIX = "Bearer ";

    public static final String LOGIN_TYPE_UAA = "uaa";

    public static final String LOGIN_TYPE_LOCAL = "local";

    public static final String JWT_PRINCIPAL = "principal";
    public static final String JWT_CREDENTIALS = "credentials";
    public static final String JWT_AUTHORITIES = "authorities";

    public static final String CONTENT_TYPE = "application/json;charset=utf-8";

    /**
     * local登录方式，允许不验证的路径，以,分隔
     */
    public static final String PATHS_PERMIT_LOCAL = "/api/sysUser/findByUsername";

    /**
     * Jwt token中包含的key
     */
    public static final String USER_ID = "user_id";

    public static final String CHARSET = "UTF-8";

    public static final String RSA_ALGORITHM = "RSA";

    public static final String JKS_ALIAS = "oriole";
    public static final String JKS_KEYPASS = "oriolepass";
    public static final String JKS_KEYSTORE = "oriole.jks";
    public static final String JKS_STOREPASS = "oriolepass";

    public static final String CAPTCHA_MSG = "验证码输入不正确";

    public static final Integer TOKEN_EXPIRED_CODE = -998;

    public static final String TOKEN_EXPIRED_MSG = "TOKEN_EXPIRED";

    /**
     * 通用成功标识
     */
    public static final String SUCCESS = "0";

    /**
     * 通用失败标识
     */
    public static final String FAIL = "1";
    public static final String SESSION_KEY_VERIFYCODE = "verifyCode";

    /**
     * cron表达式不正确
     */
    public static final String CORN_INVALID = "CORN_INVALID";

    /**
     * 字典类型已经存在
     * Dictionary Type already exists
     */
    public static final String DICTIONARY_TYPE_ALREADY_EXISTS = "DICTIONARY_TYPE_ALREADY_EXISTS";

    public static final String ID_CANNOT_BE_NULL = "ID_CANNOT_BE_NULL";

    public static final String SYS_TYPE_HAS_BEEN_USED = "SYS_TYPE_HAS_BEEN_USED";

    public static final String REQUEST_TIME_OUT = "REQUEST_TIME_OUT";

}
