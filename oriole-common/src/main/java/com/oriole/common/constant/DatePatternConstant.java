package com.oriole.common.constant;

import cn.hutool.core.date.DateUtil;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/8/5 21:29
 * description:
 */
public class DatePatternConstant {

    public static final String yyyyMMdd = "yyyy-MM-dd";

    public static final String yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss";
}
