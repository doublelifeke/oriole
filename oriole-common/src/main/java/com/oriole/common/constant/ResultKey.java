package com.oriole.common.constant;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/5 23:13
 * Description: message 国际化
 */
public class ResultKey {

    /**
     * 成功
     */
    public static final String SUCCESS = "SUCCESS";

    /**
     * 失败
     */
    public static final String FAIL = "FAIL";

    /**
     * 用户不存在
     */
    public static final String USER_DOES_NOT_EXIST = "USER_DOES_NOT_EXIST";

    /**
     * 密码不匹配
     */
    public static final String PASSWORD_MISMATCH = "PASSWORD_MISMATCH";

    /**
     * 新旧密码不能相同
     * The new password cannot be the same as the old password
     */
    public static final String NEW_OLD_PASSWORD_NOT_SAME = "NEW_OLD_PASSWORD_NOT_SAME";

    /**
     * 菜单关联角色
     */
    public static final String MENU_ASSOCIATED_ROLES = "MENU_ASSOCIATED_ROLES";

    /**
     * 快捷菜单关联用户
     */
    public static final String SHORTCUT_MENU_ASSOCIATED_ROLES = "SHORTCUT_MENU_ASSOCIATED_ROLES";

}
