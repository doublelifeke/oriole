package com.oriole.common.constant;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2019/3/20
 * Time: 15:50
 */
public class ResultModel<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private int value = ResultStatus.SUCCESS.getValue();

    private String message = ResultStatus.SUCCESS.getMessage();

    private T data;

    public ResultModel() {

    }

    public ResultModel(int value, String message) {
        this.value = value;
        this.message = message;
        this.data = null;
    }

    public ResultModel(int value, String message, T data) {
        this.value = value;
        this.message = message;
        this.data = data;
    }

    private ResultModel(T data) {
        this.data = data;
    }

    public ResultModel(ResultStatus resultStatus) {
        this.value = resultStatus.getValue();
        this.message = resultStatus.getMessage();
        this.data = null;
    }

    public ResultModel(ResultStatus resultStatus, T data) {
        this.value = resultStatus.getValue();
        this.message = resultStatus.getMessage();
        this.data = data;
    }

    public ResultModel(HttpStatus httpStatus) {
        this.value = httpStatus.value();
        this.message = httpStatus.getReasonPhrase();
        this.data = null;
    }

    public ResultModel(HttpStatus httpStatus, T data) {
        this.value = httpStatus.value();
        this.message = httpStatus.getReasonPhrase();
        this.data = data;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static ResultModel<Object> success() {
        return new ResultModel<>();
    }

    public static <T> ResultModel<T> success(String message) {
        return new ResultModel<>(ResultStatus.SUCCESS.getValue(), message);
    }

    public static <T> ResultModel<T> success(T data) {
        return new ResultModel<>(data);
    }

    public static <T> ResultModel<T> success(String message, T data) {
        return new ResultModel<>(ResultStatus.SUCCESS.getValue(), message, data);
    }

    public static ResultModel<Object> fail() {
        return new ResultModel<>(ResultStatus.FAIL);
    }

    public static <T> ResultModel<T> fail(String message) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), message);
    }

    public static <T> ResultModel<T> fail(T data) {
        return new ResultModel<>(ResultStatus.FAIL, data);
    }

    public static <T> ResultModel<T> fail(String message, T data) {
        return new ResultModel<>(ResultStatus.FAIL.getValue(), message, data);
    }

}
