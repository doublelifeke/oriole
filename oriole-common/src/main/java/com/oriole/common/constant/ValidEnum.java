package com.oriole.common.constant;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/7/29 22:46
 * description:
 */
public enum ValidEnum {
    /**
     * 有效
     */
    Y("YES"),
    /**
     * 无效
     */
    N("NO");

    private String message;

    ValidEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
