package com.oriole.common.constant;

import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2019/3/20
 * Time: 15:45
 */
public enum ResultStatus {

    /**
     * 成功
     */
    SUCCESS(200, ResultKey.SUCCESS),

    /**
     * 失败
     */
    FAIL(-999, ResultKey.FAIL);

    private int value;

    private String message;

    ResultStatus(int value, String message) {
        this.value = value;
        this.message = message;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
