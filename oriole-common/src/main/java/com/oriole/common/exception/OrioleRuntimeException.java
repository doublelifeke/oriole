package com.oriole.common.exception;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/3/10
 * Time: 20:24 oriole 运行时异常
 */
public class OrioleRuntimeException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public OrioleRuntimeException() {
        super();
    }

    public OrioleRuntimeException(String message) {
        super(message);
    }

    public OrioleRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public OrioleRuntimeException(Throwable cause) {
        super(cause);
    }

    protected OrioleRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
