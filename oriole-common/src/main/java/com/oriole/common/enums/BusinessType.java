package com.oriole.common.enums;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/7 21:25
 * Description:
 */
public enum BusinessType {

    /**
     * 其它
     */
    OTHER,

    /**
     * 新增
     */
    INSERT,

    /**
     * 删除
     */
    DELETE,

    /**
     * 修改
     */
    UPDATE,

    /**
     * 查询
     */
    SELECT,

    /**
     * 页面
     */
    TO_PAGE,

}
