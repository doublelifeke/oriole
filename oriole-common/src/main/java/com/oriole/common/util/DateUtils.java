package com.oriole.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/26 22:51
 * Description:
 */
public class DateUtils {

    public static final String DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String yyyyMMdd = "yyyy-MM-dd";

    public static final String yyyyMMddNo = "yyyyMMdd";

    /**
     * String日期 转换成 Date日期
     *
     * @param strDate 待转换的日期格式字符串
     * @param format  待转换的日期格式
     * @return 日期格式的字符串转日期
     */
    public static Date strToDate(String strDate, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        try {
            return dateFormat.parse(strDate);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * String日期 转换成 Date日期
     *
     * @param stringDate 待转换的日期格式字符串
     * @return 日期格式的字符串转日期
     */
    public static Date defaultStrToDate(String stringDate) {
        return strToDate(stringDate, DEFAULT_FORMAT);
    }

    /**
     * Date日期 转换成 String日期
     *
     * @param date   待转换的日期
     * @param format 待转换的字符串日期格式
     * @return 日期转字符串
     */
    public static String dateToStr(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    /**
     * Date日期 转换成 String日期
     *
     * @param date 待转换的日期
     * @return 日期转字符串
     */
    public static String defaultDateToStr(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_FORMAT);
        return dateFormat.format(date);
    }

    public static Date getStartDay(String strDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(yyyyMMdd);
        try {
            Date date = dateFormat.parse(strDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            return calendar.getTime();
        } catch (ParseException e) {
            return new Date();
        }
    }

    public static Date getEndDay(String strDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(yyyyMMdd);
        try {
            Date date = dateFormat.parse(strDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 999);
            return calendar.getTime();
        } catch (ParseException e) {
            return new Date();
        }
    }

    /**
     * 比较两个日期之间的大小
     *
     * @param beforeDate
     * @param afterDate
     * @return 前者大于后者返回true 反之false
     */
    public static boolean compareDate(Date beforeDate, Date afterDate) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.setTime(beforeDate);
        c2.setTime(afterDate);
        return c1.compareTo(c2) >= 0;
    }

    /**
     * 计算两个时间差
     */
    public static String getDatePoor(Date endDate, Date nowDate) {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return day + "天" + hour + "小时" + min + "分钟";
    }

    public static LocalDateTime nowByLocalDateTime() {
        return LocalDateTime.now();
    }

    public static Instant nowByInstant() {
        return Instant.now();
    }

    public static Date nowByDate() {
        return new Date();
    }

    /**
     * date转换为Instant
     *
     * @param date
     * @return
     */
    public static Instant dateToInstant(Date date) {
        return date.toInstant();
    }

    /**
     * Instant转换为date
     *
     * @param instant
     * @return
     */
    public static Date instantToDate(Instant instant) {
        return Date.from(instant);
    }

    /**
     * Date转换为LocalDateTime
     *
     * @param date
     * @return
     */
    public static LocalDateTime dateToLocalDateTime(Date date) {
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        return LocalDateTime.ofInstant(instant, zone);
    }

    /**
     * LocalDateTime转换为Date
     *
     * @param localDateTime
     * @return
     */
    public static Date localDateTimeToDate(LocalDateTime localDateTime) {
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = localDateTime.atZone(zone).toInstant();
        return Date.from(instant);
    }

    /**
     * Instant转换为LocalDateTime
     *
     * @param instant
     * @return
     */
    public static LocalDateTime instantToLocalDateTime(Instant instant) {
        ZoneId zone = ZoneId.systemDefault();
        return LocalDateTime.ofInstant(instant, zone);
    }

    /**
     * LocalDateTime转换为Instant
     *
     * @param localDateTime
     * @return
     */
    public static Instant localDateTimeToInstant(LocalDateTime localDateTime) {
        return localDateTime.atZone(ZoneId.systemDefault()).toInstant();
    }

    /**
     * Date转换为LocalDate
     *
     * @param date
     * @return
     */
    public static LocalDate dateToLocalDate(Date date) {
        return dateToLocalDateTime(date).toLocalDate();
    }

    /**
     * Date转换为LocalTime
     *
     * @param date
     * @return
     */
    public static LocalTime dateToLocalTime(Date date) {
        return dateToLocalDateTime(date).toLocalTime();
    }

    /**
     * LocalDate转换为Date
     *
     * @param localDate
     * @return
     */
    public static Date localDateToDate(LocalDate localDate) {
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = localDate.atStartOfDay().atZone(zone).toInstant();
        return Date.from(instant);
    }

    /**
     * LocalTime转换为Date
     *
     * @param localTime
     * @return
     */
    public static Date LocalTimeToDate(LocalTime localTime) {
        LocalDate localDate = LocalDate.now();
        LocalDateTime localDateTime = LocalDateTime.of(localDate, localTime);
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = localDateTime.atZone(zone).toInstant();
        return Date.from(instant);
    }

    /**
     * Instant转换为 String
     *
     * @param instant
     * @return
     */
    public static String instantToStr(Instant instant) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DEFAULT_FORMAT)
                .withZone(ZoneId.systemDefault())
                .withLocale(Locale.CHINA);
        return formatter.format(instant);
    }

    /**
     * String转换为Instant
     *
     * @param str
     * @return
     */
    public static Instant strToInstant(String str) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DEFAULT_FORMAT)
                .withZone(ZoneId.systemDefault())
                .withLocale(Locale.CHINA);
        return LocalDateTime.parse(str, formatter)
                .toInstant(ZoneOffset.UTC);
    }

    /**
     * String转换为LocalDateTime
     *
     * @param str
     * @return
     */
    public static LocalDateTime strToLocalDateTime(String str) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DEFAULT_FORMAT);
        return LocalDateTime.parse(str, formatter);
    }

    /**
     * LocalDateTime转换为String
     *
     * @param localDateTime
     * @return
     */
    public static String localDateTimeToStr(LocalDateTime localDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DEFAULT_FORMAT);
        return formatter.format(localDateTime);
    }


}
