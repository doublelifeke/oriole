package com.oriole.common.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/8/19 20:00
 * description: Oriole密码加密方法
 */
public class PasswordEncoderUtil {

    /**
     * 通过BCrypt加密源密码
     *
     * @param rawPassword 源密码
     * @return 加密后密码
     */
    public static String encodeByBCrypt(String rawPassword) {
        return new BCryptPasswordEncoder().encode(rawPassword);
    }

    /**
     * 密码比对
     *
     * @param rawPassword     源密码
     * @param encodedPassword 加密后的密码
     * @return 一致返回true，否则返回false
     */
    public static boolean matchesByBCrypt(CharSequence rawPassword, String encodedPassword) {
        return new BCryptPasswordEncoder().matches(rawPassword, encodedPassword);
    }

}
