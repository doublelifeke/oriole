package com.oriole.common.util;

import com.oriole.common.exception.OrioleRuntimeException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Email: hautxxxyzjk@163.com
 * Date: 2019/6/9 0:46
 * Description:
 */
public class RsaFileUtils {

    /**
     * 读取证书的字符串内容
     *
     * @param path     路径
     * @param keywords 关键字 1. PUBLIC KEY  2. PRIVATE KEY
     * @return 证书的字符串内容
     */
    public static String getContent(String path, String keywords) {
        InputStream resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
        if (resourceAsStream == null) {
            throw new OrioleRuntimeException("No certificate with the keyword [" + keywords + "] in this path: " + path);
        }
        StringBuilder key = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(resourceAsStream));
            String line;
            while (null != (line = br.readLine())) {
                if ((line.contains(keywords))) {
                    continue;
                }
                key.append(line);
            }
            return key.toString();
        } catch (IOException e) {
            throw new OrioleRuntimeException("IO read exception!");
        }
    }

}
