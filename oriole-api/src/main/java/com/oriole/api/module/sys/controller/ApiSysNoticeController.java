package com.oriole.api.module.sys.controller;

import com.oriole.api.module.sys.service.ISysDictionaryDataService;
import com.oriole.api.module.sys.service.ISysNoticeService;
import com.oriole.common.constant.Constants;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.common.exception.OrioleRuntimeException;
import com.oriole.entity.sys.SysDictionaryData;
import com.oriole.entity.sys.SysNotice;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/12/13 13:44
 * Description:
 */
@Api(value = "ApiSysNoticeController", tags = {"系统通知-ApiSysNoticeController"})
@RestController
@RequestMapping("/api/sysNotice")
public class ApiSysNoticeController {

    @Resource
    private ISysNoticeService sysNoticeService;

    @Resource
    private ISysDictionaryDataService sysDictionaryDataService;

    @ApiOperation(value = "系统通知-获取系统通知列表数据")
    @GetMapping("/findPage")
    public ResultModel<Object> findPage(@RequestBody OriolePage oriolePage) {
        Pageable pageable = PageableUtil.getPageable(oriolePage);
        Page<SysNotice> page = sysNoticeService.findPage(oriolePage.getCondition(), pageable);
        return ResultModel.success(page);
    }

    @ApiOperation(value = "系统通知-保存")
    @PostMapping("/save")
    public ResultModel<SysNotice> save(@ApiParam @Valid @RequestBody SysNotice sysNotice) {
        Long id = sysNotice.getSysDictionaryData().getId();
        SysDictionaryData sysDictionaryData = sysDictionaryDataService.findById(id);
        sysNotice.setSysDictionaryData(sysDictionaryData);
        SysNotice result = sysNoticeService.save(sysNotice);
        return ResultModel.success(result);
    }

    @ApiOperation(value = "系统通知-根据ID获取信息")
    @GetMapping("/find/{id}")
    public ResultModel<SysNotice> findById(@PathVariable("id") Long id) {
        SysNotice sysNotice = sysNoticeService.findById(id);
        return ResultModel.success(sysNotice);
    }

    @ApiOperation(value = "系统通知-编辑")
    @PutMapping("/update")
    public ResultModel<SysNotice> update(@Valid @RequestBody SysNotice sysNotice) {
        if (StringUtils.isBlank(sysNotice.getId().toString())) {
            throw new OrioleRuntimeException(Constants.ID_CANNOT_BE_NULL);
        }
        SysNotice persistentSysNotice = sysNoticeService.findById(sysNotice.getId());
        BeanUtils.copyProperties(sysNotice, persistentSysNotice, "createdBy", "createdDate");
        Long id = sysNotice.getSysDictionaryData().getId();
        SysDictionaryData sysDictionaryData = sysDictionaryDataService.findById(id);
        sysNotice.setSysDictionaryData(sysDictionaryData);
        persistentSysNotice.setSysDictionaryData(sysDictionaryData);
        SysNotice result = sysNoticeService.save(persistentSysNotice);
        return ResultModel.success(result);
    }

    @ApiOperation(value = "系统通知-删除")
    @DeleteMapping("/delete/{id}")
    public ResultModel<Object> delete(@PathVariable("id") Long id) {
        sysNoticeService.deleteById(id);
        return ResultModel.success();
    }

}
