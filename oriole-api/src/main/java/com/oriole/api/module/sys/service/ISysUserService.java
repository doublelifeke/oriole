package com.oriole.api.module.sys.service;

import com.oriole.entity.sys.SysUser;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统用户表 服务类
 * </p>
 *
 * @author hautxxxyzjk@163.com
 * @since 2020-03-10
 */
public interface ISysUserService {

    SysUser save(SysUser sysUser);

    void deleteById(Long id);

    void deleteBatchByIds(List<Long> ids);

    List<SysUser> findAll();

    Page<SysUser> findPage(Map<String, Object> condition, Pageable pageable);

    SysUser findByUsername(String username);

    SysUser findById(Long id);

}
