package com.oriole.api.module.sys.service;

import com.oriole.entity.sys.SysDept;
import com.oriole.entity.sys.SysUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/8 20:46
 * Description:
 */
public interface ISysDeptService {

    List<SysDept> findAll();

    SysDept findById(Long id);

    Page<SysDept> findPage(Map<String, Object> condition, Pageable pageable);

    SysDept save(SysDept sysDept);

    void deleteById(Long id);
}
