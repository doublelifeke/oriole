package com.oriole.api.module.sys.service.impl;

import com.oriole.api.module.sys.repository.SysMenuRepository;
import com.oriole.api.module.sys.repository.SysRoleRepository;
import com.oriole.api.module.sys.repository.SysUserRepository;
import com.oriole.api.module.sys.service.ISysMenuService;
import com.oriole.common.constant.ResultKey;
import com.oriole.common.constant.ValidEnum;
import com.oriole.entity.sys.*;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import liquibase.pro.packaged.S;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/9/28 21:43
 * Description:
 */
@Service
public class SysMenuServiceImpl implements ISysMenuService {

    @Resource
    private SysMenuRepository sysMenuRepository;

    @Resource
    private SysUserRepository sysUserRepository;

    @Resource
    private JPAQueryFactory queryFactory;

    @Resource
    private SysRoleRepository sysRoleRepository;

    @Override
    public List<SysMenu> findByUserId(Long userId) {
        QSysMenu sysMenu = QSysMenu.sysMenu;
        SysUser sysUser = sysUserRepository.findById(userId).orElse(new SysUser());
        if (ValidEnum.Y.equals(sysUser.getIsSuper())) {
            return queryFactory.select(sysMenu).from(sysMenu).fetch();
        }
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        booleanBuilder.and(sysMenu.sysRoles.any().sysUsers.any().id.eq(userId));
        return queryFactory.select(sysMenu).from(sysMenu).where(booleanBuilder).fetch();
    }

    @Override
    public List<SysMenu> findShortcutByUserId(Long userId) {
        QSysMenu sysMenu = QSysMenu.sysMenu;
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        booleanBuilder.and(sysMenu.sysUsers.any().id.eq(userId));
        return queryFactory.select(sysMenu).from(sysMenu).where(booleanBuilder).fetch();
    }

    @Override
    public List<SysMenu> findAll() {
        return sysMenuRepository.findAll();
    }

    @Override
    public SysMenu save(SysMenu sysMenu) {
        return sysMenuRepository.save(sysMenu);
    }

    @Override
    public SysMenu findById(Long id) {
        return sysMenuRepository.findById(id).orElse(new SysMenu());
    }

    @Override
    public List<String> deleteById(Long id) {
        List<SysMenu> childrenSysMenu = sysMenuRepository.findByParentId(id);
        List<Long> ids = new ArrayList<>();
        childrenSysMenu.forEach(sysMenu -> ids.add(sysMenu.getId()));
        ids.add(id);

        List<String> result = new ArrayList<>();
        // 先判断关联的信息
        Iterable<SysRole> sysRoles = sysRoleRepository.findAll(new BooleanBuilder(QSysRole.sysRole.sysMenus.any().id.in(ids)));
        if (sysRoles.iterator().hasNext()) {
            result.add(ResultKey.MENU_ASSOCIATED_ROLES);
        }
        Iterable<SysUser> sysUsers = sysUserRepository.findAll(new BooleanBuilder(QSysUser.sysUser.sysMenus.any().id.in(ids)));
        if (sysUsers.iterator().hasNext()) {
            result.add(ResultKey.SHORTCUT_MENU_ASSOCIATED_ROLES);
        }
        if (result.isEmpty()) {
            // 删除当前菜单和子级菜单
            sysMenuRepository.deleteByParentId(id);
            sysMenuRepository.deleteById(id);
        }
        return result;
    }

    /**
     * 根据角色sysRoleID查询菜单
     *
     * @param sysRoleId 角色ID
     * @return 结果
     */
    @Override
    public List<SysMenu> findBySysRoleId(Long sysRoleId) {
        QSysMenu sysMenu = QSysMenu.sysMenu;
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        booleanBuilder.and(sysMenu.sysRoles.any().id.eq(sysRoleId));
        return queryFactory.select(sysMenu).from(sysMenu).where(booleanBuilder).fetch();
    }

}
