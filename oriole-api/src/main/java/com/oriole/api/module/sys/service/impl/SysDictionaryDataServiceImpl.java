package com.oriole.api.module.sys.service.impl;

import com.google.common.collect.Lists;
import com.oriole.api.module.sys.repository.SysDictionaryDataRepository;
import com.oriole.api.module.sys.service.ISysDictionaryDataService;
import com.oriole.common.util.DateUtils;
import com.oriole.entity.sys.QSysDictionaryData;
import com.oriole.entity.sys.SysDictionaryData;
import com.querydsl.core.BooleanBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/2 19:54
 * Description:
 */
@Service
public class SysDictionaryDataServiceImpl implements ISysDictionaryDataService {

    @Resource
    private SysDictionaryDataRepository sysDictionaryDataRepository;

    @Override
    public Page<SysDictionaryData> findPage(Map<String, Object> condition, Pageable pageable) {
        if (null != condition && !condition.isEmpty()) {
            BooleanBuilder booleanBuilder = new BooleanBuilder();
            QSysDictionaryData sysDictionaryType = QSysDictionaryData.sysDictionaryData;
            if (condition.containsKey("label")) {
                String label = condition.get("label").toString();
                booleanBuilder.and(sysDictionaryType.label.likeIgnoreCase("%" + label + "%"));
            }
            if (condition.containsKey("createdDateStart")) {
                String createdDateStart = condition.get("createdDateStart").toString();
                Date startDay = DateUtils.getStartDay(createdDateStart);
                booleanBuilder.and(sysDictionaryType.createdDate.goe(startDay));
            }
            if (condition.containsKey("createdDateEnd")) {
                String createdDateEnd = condition.get("createdDateEnd").toString();
                Date endDay = DateUtils.getEndDay(createdDateEnd);
                booleanBuilder.and(sysDictionaryType.createdDate.loe(endDay));
            }
            return sysDictionaryDataRepository.findAll(booleanBuilder, pageable);
        }
        return sysDictionaryDataRepository.findAll(pageable);
    }

    @Override
    public List<SysDictionaryData> findByTypeId(Long id) {
        QSysDictionaryData sysDictionaryData = QSysDictionaryData.sysDictionaryData;

        BooleanBuilder booleanBuilder = new BooleanBuilder();
        booleanBuilder.and(sysDictionaryData.sysDictionaryType.id.eq(id));

        return Lists.newArrayList(sysDictionaryDataRepository.findAll(booleanBuilder));
    }

    @Override
    public SysDictionaryData findById(Long id) {
        return sysDictionaryDataRepository.findById(id).orElse(new SysDictionaryData());
    }

    @Override
    public SysDictionaryData save(SysDictionaryData sysDictionaryData) {
        return sysDictionaryDataRepository.save(sysDictionaryData);
    }

    @Override
    public void deleteById(Long id) {
        sysDictionaryDataRepository.deleteById(id);
    }
}
