package com.oriole.api.module.sys.service;

import com.oriole.common.exception.TaskException;
import com.oriole.entity.sys.SysJob;
import org.quartz.SchedulerException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/9/20 22:47
 * description:
 */
public interface ISysJobService {

    SysJob findById(Long id);

    /**
     * 删除任务后，所对应的trigger也将被删除
     *
     * @param id 任务ID
     * @throws SchedulerException
     */
    void deleteById(Long id) throws SchedulerException;

    /**
     * 批量删除调度信息
     *
     * @param ids 需要删除的数据ID
     * @throws SchedulerException
     */
    void deleteJobByIds(String ids) throws SchedulerException;

    /**
     * 任务调度状态修改
     *
     * @param sysJob 调度信息
     * @throws SchedulerException
     */
    void changeStatus(SysJob sysJob) throws SchedulerException;

    /**
     * 立即运行任务
     *
     * @param id 任务ID
     * @throws SchedulerException
     */
    void run(Long id) throws SchedulerException;

    /**
     * 新增任务
     *
     * @param job 调度信息
     * @throws SchedulerException
     * @throws TaskException
     * @return 结果
     */
    SysJob save(SysJob job) throws SchedulerException, TaskException;

    /**
     * 更新任务
     *
     * @param sysJob 调度信息
     * @throws SchedulerException
     * @throws TaskException
     * @return 结果
     */
    SysJob update(SysJob sysJob) throws SchedulerException, TaskException;

    Page<SysJob> findPage(Map<String, Object> condition, Pageable pageable);

}
