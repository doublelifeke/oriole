package com.oriole.api.module.sys.controller;

import com.oriole.api.module.sys.service.ISysDictionaryDataService;
import com.oriole.api.module.sys.service.ISysDictionaryTypeService;
import com.oriole.common.constant.Constants;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.common.exception.OrioleRuntimeException;
import com.oriole.common.util.OrioleStringUtils;
import com.oriole.entity.sys.SysConfig;
import com.oriole.entity.sys.SysDictionaryData;
import com.oriole.entity.sys.SysDictionaryType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/2 19:52
 * Description:
 */
@Api(value = "ApiSysDictionaryDataController", tags = {"字典数据-ApiSysDictionaryDataController"})
@RestController
@RequestMapping("/api/sysDictionaryData")
public class ApiSysDictionaryDataController {

    @Resource
    private ISysDictionaryDataService sysDictionaryDataService;

    @Resource
    private ISysDictionaryTypeService sysDictionaryTypeService;

    @ApiOperation(value = "字典数据-获取列表分页")
    @GetMapping("/findPage")
    public ResultModel<Object> findPage(@RequestBody OriolePage oriolePage) {
        Pageable pageable = PageableUtil.getPageable(oriolePage);
        Page<SysDictionaryData> page = sysDictionaryDataService.findPage(oriolePage.getCondition(), pageable);
        return ResultModel.success(page);
    }

    @ApiOperation(value = "字典数据-根据ID获取用户信息")
    @GetMapping("/find/{id}")
    public ResultModel<SysDictionaryData> findById(@PathVariable("id") Long id) {
        SysDictionaryData sysDictionaryData = sysDictionaryDataService.findById(id);
        return ResultModel.success(sysDictionaryData);
    }

    @ApiOperation(value = "字典数据-新增")
    @PostMapping("/save")
    public ResultModel<SysDictionaryData> save(@Valid @RequestBody SysDictionaryData sysDictionaryData) {
        Long sysDictionaryTypeId = sysDictionaryData.getSysDictionaryType().getId();
        if (OrioleStringUtils.isNotNull(sysDictionaryTypeId)) {
            SysDictionaryType sysDictionaryType = sysDictionaryTypeService.findById(sysDictionaryTypeId);
            sysDictionaryData.setSysDictionaryType(sysDictionaryType);
        }
        SysDictionaryData result = sysDictionaryDataService.save(sysDictionaryData);
        return ResultModel.success(result);
    }

    @ApiOperation(value = "字典数据-编辑")
    @PutMapping("/update")
    public ResultModel<SysDictionaryData> update(@Valid @RequestBody SysDictionaryData sysDictionaryData) {
        if (StringUtils.isBlank(sysDictionaryData.getId().toString())) {
            throw new OrioleRuntimeException(Constants.ID_CANNOT_BE_NULL);
        }
        SysDictionaryData persistentSysDictionaryData = sysDictionaryDataService.findById(sysDictionaryData.getId());
        BeanUtils.copyProperties(sysDictionaryData, persistentSysDictionaryData, "createdBy", "createdDate");
        Long sysDictionaryTypeId = sysDictionaryData.getSysDictionaryType().getId();
        if (OrioleStringUtils.isNotNull(sysDictionaryTypeId)) {
            SysDictionaryType sysDictionaryType = sysDictionaryTypeService.findById(sysDictionaryTypeId);
            persistentSysDictionaryData.setSysDictionaryType(sysDictionaryType);
        }
        SysDictionaryData result = sysDictionaryDataService.save(persistentSysDictionaryData);
        return ResultModel.success(result);
    }

    @ApiOperation(value = "字典数据-删除")
    @DeleteMapping("/delete/{id}")
    public ResultModel<Object> delete(@PathVariable("id") Long id) {
        sysDictionaryDataService.deleteById(id);
        return ResultModel.success();
    }

    @ApiOperation(value = "字典数据-根据typeId获取信息")
    @GetMapping("/findByTypeId")
    public ResultModel<List<SysDictionaryData>> findByTypeId(@RequestParam("typeId") Long typeId) {
        List<SysDictionaryData> sysDictionaryDatas = sysDictionaryDataService.findByTypeId(typeId);
        return ResultModel.success(sysDictionaryDatas);
    }

}
