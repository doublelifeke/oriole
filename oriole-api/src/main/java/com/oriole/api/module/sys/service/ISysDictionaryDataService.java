package com.oriole.api.module.sys.service;

import com.oriole.entity.sys.SysDictionaryData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/2 19:53
 * Description:
 */
public interface ISysDictionaryDataService {

    Page<SysDictionaryData> findPage(Map<String, Object> condition, Pageable pageable);

    List<SysDictionaryData> findByTypeId(Long id);

    SysDictionaryData findById(Long id);

    SysDictionaryData save(SysDictionaryData sysDictionaryData);

    void deleteById(Long id);
}
