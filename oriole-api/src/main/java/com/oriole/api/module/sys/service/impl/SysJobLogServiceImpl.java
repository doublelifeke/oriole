package com.oriole.api.module.sys.service.impl;

import com.oriole.api.module.sys.repository.SysJobLogRepository;
import com.oriole.api.module.sys.service.ISysJobLogService;
import com.oriole.common.util.ConvertUtil;
import com.oriole.common.util.DateUtils;
import com.oriole.entity.sys.QSysJob;
import com.oriole.entity.sys.QSysJobLog;
import com.oriole.entity.sys.SysJobLog;
import com.querydsl.core.BooleanBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/9/20 22:46
 * description:
 */
@Service
public class SysJobLogServiceImpl implements ISysJobLogService {

    @Resource
    private SysJobLogRepository sysJobLogRepository;

    @Override
    public SysJobLog save(SysJobLog jobLog) {
        return sysJobLogRepository.save(jobLog);
    }

    @Override
    public void deleteJobLogByIds(String ids) {
        Long[] longs = ConvertUtil.toLongArray(ids);
        for (Long id : longs) {
            sysJobLogRepository.deleteById(id);
        }
    }

    @Override
    public void deleteById(Long id) {
        sysJobLogRepository.deleteById(id);
    }

    @Override
    public SysJobLog findById(Long id) {
        return sysJobLogRepository.findById(id).orElse(new SysJobLog());
    }

    @Override
    public Page<SysJobLog> findPage(Map<String, Object> condition, Pageable pageable) {
        if (null != condition && !condition.isEmpty()) {
            BooleanBuilder booleanBuilder = new BooleanBuilder();
            QSysJobLog sysJobLog = QSysJobLog.sysJobLog;
            if (condition.containsKey("jobName")) {
                String jobName = condition.get("jobName").toString();
                booleanBuilder.and(sysJobLog.jobName.likeIgnoreCase("%"+jobName+"%"));
            }
            if (condition.containsKey("createdDateStart")) {
                String createdDateStart = condition.get("createdDateStart").toString();
                Date startDay = DateUtils.getStartDay(createdDateStart);
                booleanBuilder.and(sysJobLog.createdDate.goe(startDay));
            }
            if (condition.containsKey("createdDateEnd")) {
                String createdDateEnd = condition.get("createdDateEnd").toString();
                Date endDay = DateUtils.getEndDay(createdDateEnd);
                booleanBuilder.and(sysJobLog.createdDate.loe(endDay));
            }
            return sysJobLogRepository.findAll(booleanBuilder, pageable);
        }
        return sysJobLogRepository.findAll(pageable);
    }
}
