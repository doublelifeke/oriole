package com.oriole.api.module.sys.repository;

import com.oriole.entity.sys.SysRole;
import com.oriole.entity.sys.SysUser;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/9/28 21:45
 * Description:
 */
public interface SysUserRepository extends JpaRepository<SysUser, Long>, JpaSpecificationExecutor<SysUser>, QuerydslPredicateExecutor<SysUser> {

    @EntityGraph(attributePaths = {"sysRoles", "sysDept", "sysRoles.sysMenus"})
    SysUser findByUsername(String username);
}
