package com.oriole.api.module.sys.service;

import com.oriole.entity.sys.SysFile;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/31 14:32
 * Description:
 */
public interface ISysFileService {

    SysFile save(SysFile sysFile);

}
