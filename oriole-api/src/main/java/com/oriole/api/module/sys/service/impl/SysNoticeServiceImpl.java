package com.oriole.api.module.sys.service.impl;

import com.oriole.api.module.sys.repository.SysNoticeRepository;
import com.oriole.api.module.sys.service.ISysNoticeService;
import com.oriole.common.constant.ValidEnum;
import com.oriole.entity.sys.SysDictionaryData;
import com.oriole.entity.sys.SysNotice;
import com.oriole.entity.sys.SysUser;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/12/13 13:51
 * Description:
 */
@Service
public class SysNoticeServiceImpl implements ISysNoticeService {

    @Resource
    private SysNoticeRepository sysNoticeRepository;

    @Override
    public Page<SysNotice> findPage(Map<String, Object> condition, Pageable pageable) {
        if (null != condition && !condition.isEmpty()) {
            SysNotice sysNotice = new SysNotice();
            if (condition.containsKey("title")) {
                sysNotice.setTitle(condition.get("title").toString());
            }
            ExampleMatcher matcher = ExampleMatcher.matching()
                    .withMatcher("title", ExampleMatcher.GenericPropertyMatchers.contains());
            Example<SysNotice> example = Example.of(sysNotice, matcher);
            return sysNoticeRepository.findAll(example, pageable);
        }
        return sysNoticeRepository.findAll(pageable);
    }

    @Override
    public SysNotice save(SysNotice sysNotice) {
        return sysNoticeRepository.save(sysNotice);
    }

    @Override
    public SysNotice findById(Long id) {
        return sysNoticeRepository.findById(id).orElse(new SysNotice());
    }

    @Override
    public void deleteById(Long id) {
        sysNoticeRepository.deleteById(id);
    }
}
