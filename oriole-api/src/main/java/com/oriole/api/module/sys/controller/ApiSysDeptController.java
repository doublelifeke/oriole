package com.oriole.api.module.sys.controller;

import com.oriole.api.module.sys.service.ISysDeptService;
import com.oriole.common.constant.Constants;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.constant.ResultStatus;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.common.exception.OrioleRuntimeException;
import com.oriole.entity.sys.SysDept;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/8 20:45
 * Description:
 */
@Api(value = "ApiSysDeptController", tags = {"系统部门-ApiSysConfigController"})
@RestController
@RequestMapping("/api/sysDept")
public class ApiSysDeptController {

    @Resource
    private ISysDeptService sysDeptService;

    @ApiOperation(value = "系统部门-获取列表")
    @GetMapping("/findAll")
    public ResultModel<List<SysDept>> findAll() {
        List<SysDept> sysDepts = sysDeptService.findAll();
        return ResultModel.success(sysDepts);
    }

    @ApiOperation(value = "系统部门-获取列表分页")
    @GetMapping("/findPage")
    public ResultModel<Page<SysDept>> findPage(@RequestBody OriolePage oriolePage) {
        Pageable pageable = PageableUtil.getPageable(oriolePage);
        Page<SysDept> page = sysDeptService.findPage(oriolePage.getCondition(), pageable);
        return ResultModel.success(page);
    }

    @ApiOperation(value = "系统部门-保存部门")
    @PostMapping("/save")
    public ResultModel<SysDept> save(@Valid @RequestBody SysDept sysDept) {
        SysDept result = sysDeptService.save(sysDept);
        return ResultModel.success(result);
    }

    @ApiOperation(value = "系统部门-根据ID获取部门信息")
    @GetMapping("/find/{id}")
    public ResultModel<SysDept> findById(@ApiParam(name = "id", value = "主键ID", required = true) @PathVariable("id") Long id) {
        SysDept sysDept = sysDeptService.findById(id);
        return ResultModel.success(sysDept);
    }

    @ApiOperation(value = "系统部门-修改部门信息")
    @PutMapping("/update")
    public ResultModel<SysDept> update(@ApiParam(name = "sysDept", value = "部门信息", required = true) @Valid @RequestBody SysDept sysDept) {
        if (StringUtils.isBlank(sysDept.getId().toString())) {
            throw new OrioleRuntimeException(Constants.ID_CANNOT_BE_NULL);
        }
        // 持久化对象
        SysDept persistentSysDept = sysDeptService.findById(sysDept.getId());
        BeanUtils.copyProperties(sysDept, persistentSysDept, "createdBy", "createdDate");
        SysDept result = sysDeptService.save(persistentSysDept);

        return ResultModel.success(result);
    }

    @ApiOperation(value = "系统部门-删除部门信息")
    @DeleteMapping("/delete/{id}")
    public ResultModel<SysDept> delete(@ApiParam(name = "id", value = "主键ID", required = true) @PathVariable("id") Long id) {
        sysDeptService.deleteById(id);
        return new ResultModel<>(ResultStatus.SUCCESS);
    }

}
