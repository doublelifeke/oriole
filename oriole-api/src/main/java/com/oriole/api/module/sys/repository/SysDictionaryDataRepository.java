package com.oriole.api.module.sys.repository;

import com.oriole.entity.sys.SysDictionaryData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/2 19:55
 * Description:
 */
public interface SysDictionaryDataRepository extends JpaRepository<SysDictionaryData, Long>, JpaSpecificationExecutor<SysDictionaryData>, QuerydslPredicateExecutor<SysDictionaryData> {


}
