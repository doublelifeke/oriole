package com.oriole.api.module.sys.service;

import com.oriole.entity.sys.SysOperationLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/8 17:38
 * Description:
 */
public interface ISysOperationLogService {

    SysOperationLog save(SysOperationLog sysOperationLog);

    Page<SysOperationLog> findPage(Map<String, Object> condition, Pageable pageable);

    void deleteById(Long id);
}
