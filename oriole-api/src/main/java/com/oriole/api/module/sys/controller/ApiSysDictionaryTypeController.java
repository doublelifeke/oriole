package com.oriole.api.module.sys.controller;

import com.oriole.api.module.sys.service.ISysConfigService;
import com.oriole.api.module.sys.service.ISysDictionaryDataService;
import com.oriole.api.module.sys.service.ISysDictionaryTypeService;
import com.oriole.common.constant.Constants;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.common.exception.OrioleRuntimeException;
import com.oriole.entity.sys.SysConfig;
import com.oriole.entity.sys.SysDictionaryData;
import com.oriole.entity.sys.SysDictionaryType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/2 19:51
 * Description:
 */
@Api(value = "ApiSysDictionaryTypeController", tags = {"字典类型-ApiSysDictionaryTypeController"})
@RestController
@RequestMapping("/api/sysDictionaryType")
public class ApiSysDictionaryTypeController {

    @Resource
    private ISysDictionaryTypeService sysDictionaryTypeService;

    @Resource
    private ISysDictionaryDataService sysDictionaryDataService;

    @Resource
    private ISysConfigService sysConfigService;

    @ApiOperation(value = "字典类型-获取列表分页")
    @GetMapping("/findPage")
    public ResultModel<Object> findPage(@RequestBody OriolePage oriolePage) {
        oriolePage.setSortParams(Collections.singletonList("sort,ASC"));
        Pageable pageable = PageableUtil.getPageable(oriolePage);
        Page<SysDictionaryType> page = sysDictionaryTypeService.findPage(oriolePage.getCondition(), pageable);
        return ResultModel.success(page);
    }

    @ApiOperation(value = "字典类型-新增")
    @PostMapping("/save")
    public ResultModel<SysDictionaryType> save(@Valid @RequestBody SysDictionaryType sysDictionaryType) {
        SysDictionaryType findByType = sysDictionaryTypeService.findByType(sysDictionaryType.getType());
        if (null != findByType) {
            return ResultModel.fail(Constants.DICTIONARY_TYPE_ALREADY_EXISTS);
        }
        SysDictionaryType result = sysDictionaryTypeService.save(sysDictionaryType);
        return ResultModel.success(result);
    }

    @ApiOperation(value = "字典类型-根据ID获取用户信息")
    @GetMapping("/find/{id}")
    public ResultModel<SysDictionaryType> findById(@ApiParam(name = "id", value = "主键ID", required = true) @PathVariable("id") Long id) {
        SysDictionaryType sysDictionaryType = sysDictionaryTypeService.findById(id);
        return ResultModel.success(sysDictionaryType);
    }

    @ApiOperation(value = "字典类型-编辑")
    @PutMapping("/update")
    public ResultModel<SysDictionaryType> update(@Valid @RequestBody SysDictionaryType sysDictionaryType) {
        if (StringUtils.isBlank(sysDictionaryType.getId().toString())) {
            throw new OrioleRuntimeException(Constants.ID_CANNOT_BE_NULL);
        }
        SysDictionaryType persistentSysDictionaryType = sysDictionaryTypeService.findById(sysDictionaryType.getId());
        BeanUtils.copyProperties(sysDictionaryType, persistentSysDictionaryType, "createdBy", "createdDate");
        SysDictionaryType result = sysDictionaryTypeService.save(persistentSysDictionaryType);
        return ResultModel.success(result);
    }

    @ApiOperation(value = "字典类型-删除")
    @DeleteMapping("/delete/{id}")
    public ResultModel<Object> delete(@PathVariable("id") Long id) {
        // 判断是否被使用
        List<SysDictionaryData> sysDictionaryDatas = sysDictionaryDataService.findByTypeId(id);
        SysConfig sysConfig = sysConfigService.findByTypeId(id);
        if ((null != sysDictionaryDatas && sysDictionaryDatas.size() > 0) || (null != sysConfig && null != sysConfig.getId())) {
            return ResultModel.fail(Constants.SYS_TYPE_HAS_BEEN_USED);
        }
        sysDictionaryTypeService.deleteById(id);
        return ResultModel.success();
    }

    @ApiOperation(value = "字典类型-获取列表")
    @GetMapping("/findAll")
    public ResultModel<List<SysDictionaryType>> findAll() {
        List<SysDictionaryType> sysDictionaryTypes = sysDictionaryTypeService.findAll();
        return ResultModel.success(sysDictionaryTypes);
    }

    @ApiOperation(value = "字典类型-根据type获取信息")
    @GetMapping("/findByType")
    public ResultModel<SysDictionaryType> findByType(@RequestParam("type") String type) {
        SysDictionaryType sysDictionaryType = sysDictionaryTypeService.findByType(type);
        return ResultModel.success(sysDictionaryType);
    }

}
