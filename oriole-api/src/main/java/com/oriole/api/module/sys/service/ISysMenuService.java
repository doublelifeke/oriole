package com.oriole.api.module.sys.service;

import com.oriole.entity.sys.SysMenu;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/9/28 21:35
 * Description:
 */
public interface ISysMenuService {

    /**
     * 根据userId获取菜单列表
     *
     * @param userId 用户ID
     * @return 菜单列表
     */
    List<SysMenu> findByUserId(Long userId);

    List<SysMenu> findShortcutByUserId(Long userId);

    List<SysMenu> findAll();

    SysMenu save(SysMenu sysMenu);

    SysMenu findById(Long id);

    List<String> deleteById(Long id);

    /**
     * 根据角色sysRoleID查询菜单
     *
     * @param sysRoleId 角色ID
     * @return 结果
     */
    List<SysMenu> findBySysRoleId(Long sysRoleId);
}
