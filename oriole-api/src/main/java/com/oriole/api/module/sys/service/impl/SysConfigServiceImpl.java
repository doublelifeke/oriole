package com.oriole.api.module.sys.service.impl;

import com.oriole.api.module.sys.repository.SysConfigRepository;
import com.oriole.api.module.sys.service.ISysConfigService;
import com.oriole.common.constant.ResultModel;
import com.oriole.entity.sys.SysConfig;
import com.oriole.entity.vo.SysConfigVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/4 23:29
 * Description:
 */
@Service
public class SysConfigServiceImpl implements ISysConfigService {

    @Resource
    private SysConfigRepository sysConfigRepository;

    @Override
    public SysConfigVo findSysConfigVo() {
        SysConfigVo sysConfigVo = new SysConfigVo();
        sysConfigVo.setSysName(sysConfigRepository.findByConfigKey("sys_name").getValue());
        sysConfigVo.setSysLogo(sysConfigRepository.findByConfigKey("sys_logo").getValue());
        sysConfigVo.setSysColor(sysConfigRepository.findByConfigKey("sys_color").getValue());
        sysConfigVo.setSysBottomText(sysConfigRepository.findByConfigKey("sys_bottom_text").getValue());
        sysConfigVo.setSysIndex(sysConfigRepository.findByConfigKey("sys_index").getValue());
        return sysConfigVo;
    }

    @Override
    public SysConfig findByConfigKey(String key) {
        return sysConfigRepository.findByConfigKey(key);
    }

    @Override
    public SysConfigVo update(SysConfigVo sysConfigVo) {
        SysConfig sysNameSysConfig = sysConfigRepository.findByConfigKey("sys_name");
        sysNameSysConfig.setValue(sysConfigVo.getSysName());
        sysConfigRepository.save(sysNameSysConfig);

        SysConfig sysLogoSysConfig = sysConfigRepository.findByConfigKey("sys_logo");
        sysLogoSysConfig.setValue(sysConfigVo.getSysLogo());
        sysConfigRepository.save(sysLogoSysConfig);

        SysConfig sysColorSysConfig = sysConfigRepository.findByConfigKey("sys_color");
        sysColorSysConfig.setValue(sysConfigVo.getSysColor());
        sysConfigRepository.save(sysColorSysConfig);

        SysConfig sysBottomTextSysConfig = sysConfigRepository.findByConfigKey("sys_bottom_text");
        sysBottomTextSysConfig.setValue(sysConfigVo.getSysBottomText());
        sysConfigRepository.save(sysBottomTextSysConfig);

        SysConfig sysIndexSysConfig = sysConfigRepository.findByConfigKey("sys_index");
        sysIndexSysConfig.setValue(sysConfigVo.getSysIndex());
        sysConfigRepository.save(sysIndexSysConfig);

        return sysConfigVo;
    }

    @Override
    public SysConfig findByTypeId(Long id) {
        return sysConfigRepository.findByTypeId(id);
    }
}
