package com.oriole.api.module.sys.service.impl;

import com.oriole.api.module.sys.repository.SysDeptRepository;
import com.oriole.api.module.sys.service.ISysDeptService;
import com.oriole.common.constant.ValidEnum;
import com.oriole.entity.sys.SysDept;
import com.oriole.entity.sys.SysUser;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/8 20:46
 * Description:
 */
@Service
public class SysDeptServiceImpl implements ISysDeptService {

    @Resource
    private SysDeptRepository sysDeptRepository;

    @Override
    public List<SysDept> findAll() {
        return sysDeptRepository.findAll();
    }

    @Override
    public SysDept findById(Long id) {
        return sysDeptRepository.findById(id).orElse(new SysDept());
    }

    @Override
    public Page<SysDept> findPage(Map<String, Object> condition, Pageable pageable) {
        if (null != condition && !condition.isEmpty()) {
            SysDept sysDept = new SysDept();
            if (condition.containsKey("name")) {
                sysDept.setName(condition.get("name").toString());
            }
            ExampleMatcher matcher = ExampleMatcher.matching()
                    .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains());
            Example<SysDept> example = Example.of(sysDept, matcher);
            return sysDeptRepository.findAll(example, pageable);
        }
        return sysDeptRepository.findAll(pageable);
    }

    @Override
    public SysDept save(SysDept sysDept) {
        return sysDeptRepository.save(sysDept);
    }

    @Override
    public void deleteById(Long id) {
        sysDeptRepository.deleteById(id);
    }
}
