package com.oriole.api.module.sys.service.impl;

import com.oriole.api.module.sys.repository.SysFileRepository;
import com.oriole.api.module.sys.service.ISysFileService;
import com.oriole.entity.sys.SysFile;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/31 14:33
 * Description:
 */
@Service
public class SysFileServiceImpl implements ISysFileService {

    @Resource
    private SysFileRepository sysFileRepository;

    @Override
    public SysFile save(SysFile sysFile) {
        return sysFileRepository.save(sysFile);
    }


}
