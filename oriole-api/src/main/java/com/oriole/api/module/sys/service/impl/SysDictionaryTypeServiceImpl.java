package com.oriole.api.module.sys.service.impl;

import com.oriole.api.module.sys.repository.SysConfigRepository;
import com.oriole.api.module.sys.repository.SysDictionaryDataRepository;
import com.oriole.api.module.sys.repository.SysDictionaryTypeRepository;
import com.oriole.api.module.sys.service.ISysDictionaryTypeService;
import com.oriole.common.util.DateUtils;
import com.oriole.entity.sys.*;
import com.querydsl.core.BooleanBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/2 19:54
 * Description:
 */
@Service
public class SysDictionaryTypeServiceImpl implements ISysDictionaryTypeService {

    @Resource
    private SysDictionaryTypeRepository sysDictionaryTypeRepository;

    @Override
    public Page<SysDictionaryType> findPage(Map<String, Object> condition, Pageable pageable) {
        if (null != condition && !condition.isEmpty()) {
            BooleanBuilder booleanBuilder = new BooleanBuilder();
            QSysDictionaryType sysDictionaryType = QSysDictionaryType.sysDictionaryType;
            if (condition.containsKey("name")) {
                String name = condition.get("name").toString();
                booleanBuilder.and(sysDictionaryType.name.likeIgnoreCase("%"+name+"%"));
            }
            if (condition.containsKey("createdDateStart")) {
                String createdDateStart = condition.get("createdDateStart").toString();
                Date startDay = DateUtils.getStartDay(createdDateStart);
                booleanBuilder.and(sysDictionaryType.createdDate.goe(startDay));
            }
            if (condition.containsKey("createdDateEnd")) {
                String createdDateEnd = condition.get("createdDateEnd").toString();
                Date endDay = DateUtils.getEndDay(createdDateEnd);
                booleanBuilder.and(sysDictionaryType.createdDate.loe(endDay));
            }
            return sysDictionaryTypeRepository.findAll(booleanBuilder, pageable);
        }
        return sysDictionaryTypeRepository.findAll(pageable);
    }

    @Override
    public SysDictionaryType save(SysDictionaryType sysDictionaryType) {
        return sysDictionaryTypeRepository.save(sysDictionaryType);
    }

    @Override
    public SysDictionaryType findByType(String type) {
        return sysDictionaryTypeRepository.findByType(type);
    }

    @Override
    public SysDictionaryType findById(Long id) {
        return sysDictionaryTypeRepository.findById(id).orElse(new SysDictionaryType());
    }

    @Override
    public void deleteById(Long id) {
        sysDictionaryTypeRepository.deleteById(id);
    }

    @Override
    public List<SysDictionaryType> findAll() {
        return sysDictionaryTypeRepository.findAll();
    }
}
