package com.oriole.api.module.sys.controller;

import com.oriole.api.module.sys.service.ISysJobLogService;
import com.oriole.api.module.sys.service.ISysJobService;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.entity.sys.SysJob;
import com.oriole.entity.sys.SysJobLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.quartz.SchedulerException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/1 21:07
 * Description:
 */
@Api(value = "定时任务日志ApiSysJobLogController", tags = {"定时任务日志-SysJobLog"})
@RestController
@RequestMapping("/api/sysJobLog")
public class ApiSysJobLogController {

    @Resource
    private ISysJobLogService sysJobLogService;

    @ApiOperation(value = "定时任务日志-获取任务日志列表分页")
    @GetMapping("/findPage")
    public ResultModel<Object> findPage(@RequestBody OriolePage oriolePage) {
        Pageable pageable = PageableUtil.getPageable(oriolePage);
        Page<SysJobLog> page = sysJobLogService.findPage(oriolePage.getCondition(), pageable);
        return ResultModel.success(page);
    }

    @ApiOperation(value = "定时任务日志-根据ID删除定时任务")
    @DeleteMapping("/delete/{id}")
    public ResultModel<Object> deleteById(@ApiParam(name = "id", value = "主键ID", required = true) @PathVariable("id") Long id) {
        sysJobLogService.deleteById(id);
        return ResultModel.success();
    }

}
