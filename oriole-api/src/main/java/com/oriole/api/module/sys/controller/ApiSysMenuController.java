package com.oriole.api.module.sys.controller;

import com.oriole.api.module.sys.service.ISysMenuService;
import com.oriole.common.constant.Constants;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.constant.ResultStatus;
import com.oriole.common.exception.OrioleRuntimeException;
import com.oriole.entity.sys.SysMenu;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/9/28 21:29
 * Description:
 */
@Api(value = "ApiSysJobController", tags = {"系统菜单-ApiSysJobController"})
@RestController
@RequestMapping("/api/sysMenu")
public class ApiSysMenuController {

    @Resource
    private ISysMenuService sysMenuService;

    @ApiOperation(value = "系统菜单-根据userId获取菜单列表")
    @GetMapping("/findByUserId")
    public ResultModel<List<SysMenu>> findByUserId(@RequestParam("userId") Long userId) {
        List<SysMenu> sysMenus = sysMenuService.findByUserId(userId);
        return ResultModel.success(sysMenus);
    }

    @ApiOperation(value = "系统菜单-根据userId获取快捷菜单列表")
    @GetMapping("/findShortcutByUserId")
    public ResultModel<List<SysMenu>> findShortcutByUserId(@RequestParam("userId") Long userId) {
        List<SysMenu> sysMenus = sysMenuService.findShortcutByUserId(userId);
        return ResultModel.success(sysMenus);
    }

    @ApiOperation(value = "系统菜单-获取所有菜单列表")
    @GetMapping("/findAll")
    public ResultModel<List<SysMenu>> findAll() {
        List<SysMenu> sysMenus = sysMenuService.findAll();
        return ResultModel.success(sysMenus);
    }

    @ApiOperation(value = "系统菜单-保存菜单")
    @PostMapping("/save")
    public ResultModel<SysMenu> save(@Valid @RequestBody SysMenu sysMenu) {
        SysMenu result = sysMenuService.save(sysMenu);
        return ResultModel.success(result);
    }

    @ApiOperation(value = "系统菜单-根据ID获取菜单信息")
    @GetMapping("/find/{id}")
    public ResultModel<SysMenu> findById(@ApiParam(name = "id", value = "主键ID", required = true) @PathVariable("id") Long id) {
        SysMenu sysMenu = sysMenuService.findById(id);
        return ResultModel.success(sysMenu);
    }

    @ApiOperation(value = "系统菜单-修改菜单信息")
    @PutMapping("/update")
    public ResultModel<SysMenu> update(@ApiParam(name = "sysMenu", value = "菜单信息", required = true) @Valid @RequestBody SysMenu sysMenu) {
        if (StringUtils.isBlank(sysMenu.getId().toString())) {
            throw new OrioleRuntimeException(Constants.ID_CANNOT_BE_NULL);
        }
        // 持久化对象
        SysMenu persistentSysMenu = sysMenuService.findById(sysMenu.getId());
        BeanUtils.copyProperties(sysMenu, persistentSysMenu, "createdBy", "createdDate");
        SysMenu result = sysMenuService.save(persistentSysMenu);
        return ResultModel.success(result);
    }

    @ApiOperation(value = "系统菜单-删除菜单信息")
    @DeleteMapping("/delete/{id}")
    public ResultModel<Object> delete(@ApiParam(name = "id", value = "主键ID", required = true) @PathVariable("id") Long id) {
        List<String> result = sysMenuService.deleteById(id);
        return result.size() > 0 ? ResultModel.fail(result) : new ResultModel<>(ResultStatus.SUCCESS);
    }

    @ApiOperation(value = "系统菜单-根据角色sysRoleID查询菜单")
    @GetMapping("/findBySysRoleId")
    public ResultModel<List<SysMenu>> findBySysRoleId(@RequestParam("sysRoleId") Long sysRoleId) {
        List<SysMenu> sysMenus = sysMenuService.findBySysRoleId(sysRoleId);
        return ResultModel.success(sysMenus);
    }
}
