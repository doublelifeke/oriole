package com.oriole.api.module.sys.service;

import com.oriole.entity.sys.SysRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/11 23:16
 * Description:
 */
public interface ISysRoleService {

    List<SysRole> findAll();

    SysRole findById(Long id);

    Page<SysRole> findPage(Map<String, Object> condition, Pageable pageable);

    SysRole save(SysRole sysDept);

    void deleteById(Long id);

    List<SysRole> findBySysUserId(Long sysUserId);
}
