package com.oriole.api.module.sys.repository;

import com.oriole.entity.sys.SysMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/9/28 21:45
 * Description:
 */
public interface SysMenuRepository extends JpaRepository<SysMenu, Long>, JpaSpecificationExecutor<SysMenu>, QuerydslPredicateExecutor<SysMenu> {

    List<SysMenu> findByParentId(Long parentId);

    void deleteByParentId(Long id);
}
