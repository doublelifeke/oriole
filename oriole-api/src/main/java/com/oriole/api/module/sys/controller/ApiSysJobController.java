package com.oriole.api.module.sys.controller;

import com.oriole.api.config.quartz.util.CronUtils;
import com.oriole.api.module.sys.service.ISysJobService;
import com.oriole.common.constant.Constants;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.common.exception.OrioleRuntimeException;
import com.oriole.common.exception.TaskException;
import com.oriole.entity.sys.SysJob;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.quartz.SchedulerException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/9/20 22:46
 * description: 系统job
 */
@Api(value = "定时任务ApiSysJobController", tags = {"定时任务-SysJob"})
@RestController
@RequestMapping("/api/sysJob")
public class ApiSysJobController {

    @Resource
    private ISysJobService sysJobService;

    @ApiOperation(value = "定时任务-获取任务列表分页")
    @GetMapping("/findPage")
    public ResultModel<Object> findPage(@RequestBody OriolePage oriolePage) {
        Pageable pageable = PageableUtil.getPageable(oriolePage);
        Page<SysJob> page = sysJobService.findPage(oriolePage.getCondition(), pageable);
        return ResultModel.success(page);
    }

    @ApiOperation(value = "定时任务-任务调度状态修改")
    @PutMapping("/changeStatus")
    public ResultModel<Object> changeStatus(@RequestBody SysJob sysJob) throws SchedulerException {
        sysJobService.changeStatus(sysJob);
        return ResultModel.success();
    }

    @ApiOperation(value = "定时任务-根据ID删除定时任务")
    @DeleteMapping("/delete/{id}")
    public ResultModel<Object> deleteById(@ApiParam(name = "id", value = "主键ID", required = true) @PathVariable("id") Long id)
            throws SchedulerException {
        sysJobService.deleteById(id);
        return ResultModel.success();
    }

    @ApiOperation(value = "定时任务-新增")
    @PostMapping("/save")
    public ResultModel<SysJob> save(@Valid @RequestBody SysJob sysJob)
            throws SchedulerException, TaskException {
        if (!CronUtils.isValid(sysJob.getCronExpression())) {
            return ResultModel.fail(Constants.CORN_INVALID);
        }
        SysJob result = sysJobService.save(sysJob);
        return ResultModel.success(result);
    }

    @ApiOperation(value = "定时任务-根据ID获取定时任务信息")
    @GetMapping("/find/{id}")
    public ResultModel<SysJob> findById(@ApiParam(name = "id", value = "主键ID", required = true) @PathVariable("id") Long id) {
        SysJob sysJob = sysJobService.findById(id);
        return ResultModel.success(sysJob);
    }

    @ApiOperation(value = "定时任务-编辑")
    @PutMapping("/update")
    public ResultModel<SysJob> update(@ApiParam(name = "sysJob", value = "任务信息", required = true) @Valid @RequestBody SysJob sysJob)
            throws SchedulerException, TaskException {
        if (StringUtils.isBlank(sysJob.getId().toString())) {
            throw new OrioleRuntimeException(Constants.ID_CANNOT_BE_NULL);
        }
        SysJob result = sysJobService.update(sysJob);
        return ResultModel.success(result);
    }

    @ApiOperation(value = "定时任务-任务调度立即执行一次")
    @PostMapping("/run/{id}")
    public ResultModel<Object> run(@PathVariable("id") Long id) throws SchedulerException {
        sysJobService.run(id);
        return ResultModel.success();
    }

}
