package com.oriole.api.module.sys.service;

import com.oriole.entity.sys.SysDictionaryType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/2 19:53
 * Description:
 */
public interface ISysDictionaryTypeService {

    Page<SysDictionaryType> findPage(Map<String, Object> condition, Pageable pageable);

    SysDictionaryType save(SysDictionaryType sysDictionaryType);

    SysDictionaryType findByType(String type);

    SysDictionaryType findById(Long id);

    void deleteById(Long id);

    List<SysDictionaryType> findAll();

}
