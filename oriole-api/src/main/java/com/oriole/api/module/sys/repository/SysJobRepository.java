package com.oriole.api.module.sys.repository;

import com.oriole.entity.sys.SysJob;
import com.oriole.entity.sys.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/9/28 21:45
 * Description:
 */
public interface SysJobRepository extends JpaRepository<SysJob, Long>, JpaSpecificationExecutor<SysJob>, QuerydslPredicateExecutor<SysJob> {

}
