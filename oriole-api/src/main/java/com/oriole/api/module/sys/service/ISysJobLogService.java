package com.oriole.api.module.sys.service;

import com.oriole.entity.sys.SysJob;
import com.oriole.entity.sys.SysJobLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/9/20 22:46
 * description:
 */
public interface ISysJobLogService {
    /**
     * 新增任务日志
     *
     * @param jobLog 调度日志信息
     * @return 结果
     */
    SysJobLog save(SysJobLog jobLog);

    /**
     * 删除任务日志
     *
     * @param id 调度日志ID
     */
    void deleteById(Long id);

    /**
     * 批量删除调度日志信息
     *
     * @param ids 需要删除的数据ID
     */
    void deleteJobLogByIds(String ids);

    SysJobLog findById(Long id);

    Page<SysJobLog> findPage(Map<String, Object> condition, Pageable pageable);

}
