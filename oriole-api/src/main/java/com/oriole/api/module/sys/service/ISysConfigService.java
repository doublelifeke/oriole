package com.oriole.api.module.sys.service;

import com.oriole.common.constant.ResultModel;
import com.oriole.entity.sys.SysConfig;
import com.oriole.entity.vo.SysConfigVo;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/4 23:29
 * Description:
 */
public interface ISysConfigService {

    /**
     * 系统配置基本信息
     *
     * @return 结果
     */
    SysConfigVo findSysConfigVo();

    SysConfig findByConfigKey(String key);

    SysConfigVo update(SysConfigVo sysConfigVo);

    SysConfig findByTypeId(Long id);
}
