package com.oriole.api.module.sys.service.impl;

import com.oriole.api.config.quartz.util.CronUtils;
import com.oriole.api.config.quartz.util.ScheduleUtils;
import com.oriole.api.module.sys.repository.SysJobRepository;
import com.oriole.api.module.sys.service.ISysJobService;
import com.oriole.common.constant.ScheduleConstants;
import com.oriole.common.constant.ValidEnum;
import com.oriole.common.exception.TaskException;
import com.oriole.common.util.ConvertUtil;
import com.oriole.common.util.DateUtils;
import com.oriole.entity.sys.QSysJob;
import com.oriole.entity.sys.SysJob;
import com.oriole.entity.sys.SysUser;
import com.querydsl.core.BooleanBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/9/20 22:46
 * description:
 */
@Service
public class SysJobServiceImpl implements ISysJobService {

    @Resource
    private Scheduler scheduler;

    @Resource
    private SysJobRepository sysJobRepository;

    /**
     * 项目启动时，初始化定时器
     * 主要是防止手动修改数据库导致未同步到定时任务处理
     * （注：不能手动修改数据库ID和任务组名，否则会导致脏数据）
     */
    @PostConstruct
    public void init() throws SchedulerException, TaskException {
        scheduler.clear();
        List<SysJob> sysJobs = sysJobRepository.findAll();
        for (SysJob job : sysJobs) {
            ScheduleUtils.createScheduleJob(scheduler, job);
        }
    }

    @Override
    public SysJob findById(Long id) {
        return sysJobRepository.findById(id).orElse(new SysJob());
    }

    @Override
    @Transactional(rollbackFor = SchedulerException.class)
    public void deleteById(Long id) throws SchedulerException {
        SysJob oldSysJob = findById(id);
        sysJobRepository.deleteById(id);
        scheduler.deleteJob(ScheduleUtils.getJobKey(id, oldSysJob.getJobGroup()));
    }

    @Override
    @Transactional(rollbackFor = SchedulerException.class)
    public void deleteJobByIds(String ids) throws SchedulerException {
        Long[] jobIds = ConvertUtil.toLongArray(ids);
        for (Long jobId : jobIds) {
            deleteById(jobId);
        }
    }

    @Override
    @Transactional(rollbackFor = SchedulerException.class)
    public void changeStatus(SysJob sysJob) throws SchedulerException {
        Long jobId = sysJob.getId();
        String jobGroup = sysJob.getJobGroup();
        String status = sysJob.getStatus();

        SysJob persistentSysJob = sysJobRepository.findById(jobId).orElse(new SysJob());
        persistentSysJob.setStatus(status);
        persistentSysJob.setJobGroup(jobGroup);

        if (ScheduleConstants.Status.NORMAL.getValue().equals(status)) {
            sysJobRepository.save(persistentSysJob);
            scheduler.resumeJob(ScheduleUtils.getJobKey(jobId, jobGroup));
        } else if (ScheduleConstants.Status.PAUSE.getValue().equals(status)) {
            sysJobRepository.save(persistentSysJob);
            scheduler.pauseJob(ScheduleUtils.getJobKey(jobId, jobGroup));
        }
    }

    @Override
    @Transactional(rollbackFor = SchedulerException.class)
    public void run(Long id) throws SchedulerException {
        SysJob sysJob = sysJobRepository.findById(id).orElse(new SysJob());
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put(ScheduleConstants.TASK_PROPERTIES, sysJob);
        scheduler.triggerJob(ScheduleUtils.getJobKey(id, sysJob.getJobGroup()), jobDataMap);
    }

    @Override
    @Transactional(rollbackFor = {SchedulerException.class, TaskException.class})
    public SysJob save(SysJob job) throws SchedulerException, TaskException {
        job.setStatus(ScheduleConstants.Status.PAUSE.getValue());
        SysJob result = sysJobRepository.save(job);
        if (null != result.getId()) {
            ScheduleUtils.createScheduleJob(scheduler, job);
        }
        return result;
    }

    @Override
    @Transactional(rollbackFor = {SchedulerException.class, TaskException.class})
    public SysJob update(SysJob sysJob) throws SchedulerException, TaskException {
        SysJob persistentSysJob = sysJobRepository.findById(sysJob.getId()).orElse(new SysJob());
        String jobGroup = persistentSysJob.getJobGroup();

        BeanUtils.copyProperties(sysJob, persistentSysJob, "createdBy", "createdDate");
        SysJob result = sysJobRepository.save(persistentSysJob);
        // 判断是否存在
        JobKey jobKey = ScheduleUtils.getJobKey(result.getId(), jobGroup);
        if (scheduler.checkExists(jobKey)) {
            // 防止创建时存在数据问题，先移除，然后在执行创建操作
            scheduler.deleteJob(jobKey);
        }
        ScheduleUtils.createScheduleJob(scheduler, result);
        return result;
    }

    @Override
    public Page<SysJob> findPage(Map<String, Object> condition, Pageable pageable) {
        if (null != condition && !condition.isEmpty()) {
            BooleanBuilder booleanBuilder = new BooleanBuilder();
            QSysJob sysJob = QSysJob.sysJob;
            if (condition.containsKey("jobName")) {
                String jobName = condition.get("jobName").toString();
                booleanBuilder.and(sysJob.jobName.likeIgnoreCase("%"+jobName+"%"));
            }
            if (condition.containsKey("createdDateStart")) {
                String createdDateStart = condition.get("createdDateStart").toString();
                Date startDay = DateUtils.getStartDay(createdDateStart);
                booleanBuilder.and(sysJob.createdDate.goe(startDay));
            }
            if (condition.containsKey("createdDateEnd")) {
                String createdDateEnd = condition.get("createdDateEnd").toString();
                Date endDay = DateUtils.getEndDay(createdDateEnd);
                booleanBuilder.and(sysJob.createdDate.loe(endDay));
            }
            return sysJobRepository.findAll(booleanBuilder, pageable);
        }
        return sysJobRepository.findAll(pageable);
    }
}