package com.oriole.api.module.sys.controller;

import com.oriole.api.config.thread.AsyncManager;
import com.oriole.api.module.sys.service.ISysOperationLogService;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.common.util.PasswordEncoderUtil;
import com.oriole.entity.sys.SysDept;
import com.oriole.entity.sys.SysDictionaryType;
import com.oriole.entity.sys.SysOperationLog;
import com.oriole.entity.sys.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Collections;
import java.util.TimerTask;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/8 17:38
 * Description:
 */
@Api(value = "ApiSysOperationLogController", tags = {"操作日志-ApiSysOperationLogController"})
@RestController
@RequestMapping("/api/sysOperationLog")
public class ApiSysOperationLogController {

    @Resource
    private ISysOperationLogService sysOperationLogService;

    @ApiOperation(value = "操作日志-保存")
    @PostMapping("/save")
    public ResultModel<SysOperationLog> save(@Valid @RequestBody SysOperationLog sysOperationLog) {
        SysOperationLog result = sysOperationLogService.save(sysOperationLog);
        return ResultModel.success(result);
    }

    @ApiOperation(value = "操作日志-获取列表分页")
    @GetMapping("/findPage")
    public ResultModel<Object> findPage(@RequestBody OriolePage oriolePage) {
        oriolePage.setSortParams(Collections.singletonList("createdDate,DESC"));
        Pageable pageable = PageableUtil.getPageable(oriolePage);
        Page<SysOperationLog> page = sysOperationLogService.findPage(oriolePage.getCondition(), pageable);
        return ResultModel.success(page);
    }

    @ApiOperation(value = "操作日志-根据ID删除操作日志")
    @DeleteMapping("/delete/{id}")
    public ResultModel<Object> deleteById(@PathVariable("id") Long id) {
        sysOperationLogService.deleteById(id);
        return ResultModel.success();
    }

}
