package com.oriole.api.module.sys.service;

import com.oriole.entity.sys.SysNotice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/12/13 13:51
 * Description:
 */
public interface ISysNoticeService {

    Page<SysNotice> findPage(Map<String, Object> condition, Pageable pageable);

    SysNotice save(SysNotice sysNotice);

    SysNotice findById(Long id);

    void deleteById(Long id);
}
