package com.oriole.api.module.sys.controller;

import com.oriole.api.module.sys.service.ISysMenuService;
import com.oriole.api.module.sys.service.ISysRoleService;
import com.oriole.common.constant.Constants;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.constant.ResultStatus;
import com.oriole.common.controller.OriolePage;
import com.oriole.common.controller.PageableUtil;
import com.oriole.common.exception.OrioleRuntimeException;
import com.oriole.common.util.ConvertUtil;
import com.oriole.entity.sys.SysMenu;
import com.oriole.entity.sys.SysRole;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/11 23:15
 * Description:
 */
@Api(value = "ApiSysRoleController", tags = {"系统角色-ApiSysRoleController"})
@RestController
@RequestMapping("/api/sysRole")
public class ApiSysRoleController {

    @Resource
    private ISysRoleService sysRoleService;

    @Resource
    private ISysMenuService sysMenuService;

    @ApiOperation(value = "系统角色-获取分页角色列表")
    @GetMapping("/findPage")
    public ResultModel<Page<SysRole>> findPage(@RequestBody OriolePage oriolePage) {
        Pageable pageable = PageableUtil.getPageable(oriolePage);
        Page<SysRole> page = sysRoleService.findPage(oriolePage.getCondition(), pageable);
        return ResultModel.success(page);
    }

    @ApiOperation(value = "系统角色-保存角色")
    @PostMapping("/save")
    public ResultModel<SysRole> save(@Valid @RequestBody SysRole sysRole) {
        SysRole result = sysRoleService.save(sysRole);
        return ResultModel.success(result);
    }

    @ApiOperation(value = "系统角色-根据ID获取角色信息")
    @GetMapping("/find/{id}")
    public ResultModel<SysRole> findById(@ApiParam(name = "id", value = "主键ID", required = true) @PathVariable("id") Long id) {
        SysRole sysRole = sysRoleService.findById(id);
        return ResultModel.success(sysRole);
    }

    @ApiOperation(value = "系统角色-修改角色信息")
    @PutMapping("/update")
    public ResultModel<SysRole> update(@ApiParam(name = "sysRole", value = "角色信息", required = true) @Valid @RequestBody SysRole sysRole) {
        if (StringUtils.isBlank(sysRole.getId().toString())) {
            throw new OrioleRuntimeException(Constants.ID_CANNOT_BE_NULL);
        }
        // 持久化对象
        SysRole persistentSysRole = sysRoleService.findById(sysRole.getId());
        BeanUtils.copyProperties(sysRole, persistentSysRole, "createdBy", "createdDate");
        SysRole result = sysRoleService.save(persistentSysRole);
        return ResultModel.success(result);
    }

    @ApiOperation(value = "系统角色-删除角色信息")
    @DeleteMapping("/delete/{id}")
    public ResultModel<SysRole> delete(@ApiParam(name = "id", value = "主键ID", required = true) @PathVariable("id") Long id) {
        sysRoleService.deleteById(id);
        return new ResultModel<>(ResultStatus.SUCCESS);
    }

    @ApiOperation(value = "系统角色-角色分配菜单")
    @PutMapping("/setSysMenu")
    public ResultModel<SysRole> setSysMenu(@RequestParam String sysMenuIds, @RequestParam Long sysRoleId) {
        // 角色
        SysRole sysRole = sysRoleService.findById(sysRoleId);
        // 获取所有的菜单
        Long[] menuIds = ConvertUtil.toLongArray(sysMenuIds);
        Set<SysMenu> sysMenus = new LinkedHashSet<>();
        for (long menuId : menuIds) {
            SysMenu sysMenu = sysMenuService.findById(menuId);
            sysMenus.add(sysMenu);
        }
        sysRole.setSysMenus(sysMenus);
        SysRole result = sysRoleService.save(sysRole);
        return ResultModel.success(result);
    }

    @ApiOperation(value = "系统角色-获取所有菜单列表")
    @GetMapping("/findAll")
    public ResultModel<List<SysRole>> findAll() {
        List<SysRole> sysRoles = sysRoleService.findAll();
        return ResultModel.success(sysRoles);
    }

    @ApiOperation(value = "系统角色-根据用户ID查询拥有的角色")
    @GetMapping("/findBySysUserId")
    public ResultModel<List<SysRole>> findBySysUserId(@RequestParam("sysUserId") Long sysUserId) {
        List<SysRole> sysRoles = sysRoleService.findBySysUserId(sysUserId);
        return ResultModel.success(sysRoles);
    }

}
