package com.oriole.api.module.sys.repository;

import com.oriole.entity.sys.SysConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/4 23:30
 * Description:
 */
public interface SysConfigRepository extends JpaRepository<SysConfig, Long>, JpaSpecificationExecutor<SysConfig>, QuerydslPredicateExecutor<SysConfig> {

    /**
     * 根据字段configKey查询信息
     *
     * @param configKey configKey
     * @return 结果
     */
    SysConfig findByConfigKey(String configKey);

    SysConfig findByTypeId(Long id);
}
