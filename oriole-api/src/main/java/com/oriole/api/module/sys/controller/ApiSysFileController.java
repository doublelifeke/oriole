package com.oriole.api.module.sys.controller;

import com.oriole.api.module.sys.service.ISysFileService;
import com.oriole.common.constant.ResultModel;
import com.oriole.common.constant.ResultStatus;
import com.oriole.common.util.PasswordEncoderUtil;
import com.oriole.entity.sys.SysDept;
import com.oriole.entity.sys.SysFile;
import com.oriole.entity.sys.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/31 14:32
 * Description:
 */
@Api(value = "系统文件ApiSysFileController", tags = {"系统文件-SysFile"})
@RestController
@RequestMapping("/api/sysFile")
public class ApiSysFileController {

    @Resource
    private ISysFileService sysFileService;

    @ApiOperation(value = "系统文件-保存系统文件")
    @PostMapping("/save")
    public ResultModel<SysFile> save(@ApiParam(name = "sysFile", value = "文件对象", required = true) @Valid @RequestBody SysFile sysFile) {
        SysFile result = sysFileService.save(sysFile);
        return ResultModel.success(result);
    }

}
