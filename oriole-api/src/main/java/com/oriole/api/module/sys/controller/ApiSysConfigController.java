package com.oriole.api.module.sys.controller;

import com.oriole.api.module.sys.service.ISysConfigService;
import com.oriole.common.constant.ResultModel;
import com.oriole.entity.sys.SysConfig;
import com.oriole.entity.vo.SysConfigVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/4 23:27
 * Description:
 */
@Api(value = "ApiSysConfigController", tags = {"系统配置-ApiSysConfigController"})
@RestController
@RequestMapping("/api/sysConfig")
public class ApiSysConfigController {

    @Resource
    private ISysConfigService sysConfigService;

    @ApiOperation(value = "系统配置-系统基本配置信息")
    @GetMapping("/findSysConfigVo")
    public ResultModel<SysConfigVo> findSysConfigVo() {
        SysConfigVo sysConfigVo = sysConfigService.findSysConfigVo();
        return ResultModel.success(sysConfigVo);
    }

    @ApiOperation(value = "系统配置-根据key获取配置信息")
    @GetMapping("/findByConfigKey")
    public ResultModel<SysConfig> findByConfigKey(@RequestParam("key") String key) {
        SysConfig sysConfig = sysConfigService.findByConfigKey(key);
        return ResultModel.success(sysConfig);
    }

    @ApiOperation(value = "系统配置-编辑")
    @PutMapping("/update")
    public ResultModel<SysConfigVo> update(@RequestBody SysConfigVo sysConfigVo) {
        SysConfigVo result = sysConfigService.update(sysConfigVo);
        return ResultModel.success(result);
    }

}
