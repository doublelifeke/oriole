package com.oriole.api;

import com.querydsl.jpa.impl.JPAQueryFactory;
import io.swagger.annotations.ApiModelProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.persistence.EntityManager;
import java.net.InetAddress;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/7/01
 * Time: 11:10
 * @EnableJpaAuditing //在JPA操作中启用审计功能
 * @EntityScan("com.oriole.entity") // 用于扫描JPA实体类 @Entity
 * @ComponentScan(basePackages = "com.oriole.api.module.*.*") // 用于扫描@Controller @Service
 * @EnableJpaRepositories(basePackages = "com.oriole.api.module.*.mapper")  // 用于扫描Dao @Repository
 */
@EnableConfigurationProperties({LiquibaseProperties.class})
@SpringBootApplication
@EnableDiscoveryClient
@EnableJpaAuditing
@EntityScan("com.oriole.entity")
public class ApiApplication {

    private static final Logger log = LoggerFactory.getLogger(ApiApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(ApiApplication.class, args);
    }

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${server.port}")
    private String port;

    @ApiModelProperty("让Spring管理JPAQueryFactory")
    @Bean
    public JPAQueryFactory jpaQueryFactory(EntityManager entityManager) {
        return new JPAQueryFactory(entityManager);
    }

    @Bean
    public ApplicationRunner applicationRunner() {
        return applicationArguments -> {
            log.debug("\n////////////////////////////////////////////////////////////////////\n" +
                    "//                                                                //\n" +
                    "//              " + applicationName + " launched successfully.                 //\n" +
                    "//              " + "http://" + InetAddress.getLocalHost().getHostAddress() + ":" + port + "                        //\n" +
                    "//                                                                //\n" +
                    "////////////////////////////////////////////////////////////////////");
        };
    }
}
