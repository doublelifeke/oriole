package com.oriole.api.config.quartz;

import com.oriole.api.config.quartz.util.JobInvokeUtil;
import com.oriole.entity.sys.SysJob;
import org.quartz.JobExecutionContext;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/9/20 21:12
 * description: 定时任务处理（允许并发执行）
 */
public class QuartzJobExecution extends AbstractQuartzJob {

    @Override
    protected void doExecute(JobExecutionContext context, SysJob sysJob) throws Exception {
        JobInvokeUtil.invokeMethod(sysJob);
    }
}
