package com.oriole.api.config;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @author doublelife
 * @Email: hautxxxyzjk@163.com
 * @DateTime: 2020/7/5 21:55
 * description:
 */
@Configuration
public class LiquibaseConfig {

    @Bean
    public SpringLiquibase liquibase(@Qualifier("dataSource") DataSource dataSource,
                                     LiquibaseProperties liquibaseProperties) {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource);
        liquibase.setShouldRun(liquibaseProperties.isEnabled());
        liquibase.setChangeLog(liquibaseProperties.getChangeLog());
        liquibase.setDropFirst(liquibaseProperties.isDropFirst());
        liquibase.setContexts(liquibaseProperties.getContexts());
        return liquibase;
    }

}
