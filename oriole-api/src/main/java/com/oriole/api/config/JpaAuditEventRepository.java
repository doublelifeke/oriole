package com.oriole.api.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.actuate.audit.AuditEventRepository;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/8/19 19:34
 * description: JPA审计日志
 */
@Component
public class JpaAuditEventRepository implements AuditEventRepository {

    private static final Logger log = LoggerFactory.getLogger(JpaAuditEventRepository.class);

    @Override
    public void add(AuditEvent event) {
        if (log.isDebugEnabled()) {
            log.debug("AuditEvent is {}", event.toString());
        }
    }

    @Override
    public List<AuditEvent> find(String principal, Instant after, String type) {
        if (log.isDebugEnabled()) {
            log.debug("principal is {}, after is {}, type is {}", principal, after, type);
        }
        return null;
    }
}
