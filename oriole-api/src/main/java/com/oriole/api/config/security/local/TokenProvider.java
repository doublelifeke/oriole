package com.oriole.api.config.security.local;

import com.oriole.common.constant.Constants;
import com.oriole.common.util.RsaFileUtils;
import com.oriole.common.util.RsaUtils;
import com.oriole.entity.security.OrioleUserDetails;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.security.interfaces.RSAPublicKey;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Email: hautxxxyzjk@163.com
 * Date: 2019-03-28 21:25
 * Description:
 */
public class TokenProvider implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 得到token
     *
     * @param token
     * @return 从token中根据key获取value
     */
    public Object getToken(String token, String key) {
        String publicKey = RsaFileUtils.getContent("public.txt", " PUBLIC KEY");
        RSAPublicKey rsaPublicKey = RsaUtils.getPublicKey(publicKey);
        Claims claims = Jwts.parser()
                .setSigningKey(rsaPublicKey)
                .parseClaimsJws(token)
                .getBody();
        return claims.get(key);
    }

}
