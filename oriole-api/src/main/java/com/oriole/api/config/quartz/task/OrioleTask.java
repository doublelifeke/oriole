package com.oriole.api.config.quartz.task;

import com.oriole.common.util.OrioleStringUtils;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/9/20 23:00
 * description: 定时任务调度测试
 */
@Component("orioleTask")
public class OrioleTask {

    public void multipleParams(String s, Boolean b, Long l, Double d, Integer i) {
        System.out.println(OrioleStringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void params(String params) {
        System.out.println("执行有参方法：" + params);
    }

    public void noParams() {
        System.out.println("执行无参方法");
    }
}
