package com.oriole.oauth2.server.config;

import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/8/12 22:17
 * description: 自定义AccessTokenConverter
 */
public class OrioleDefaultAccessTokenConverter extends DefaultAccessTokenConverter {

    @Override
    public void setUserTokenConverter(UserAuthenticationConverter userTokenConverter) {
        super.setUserTokenConverter(userTokenConverter);
    }
}
