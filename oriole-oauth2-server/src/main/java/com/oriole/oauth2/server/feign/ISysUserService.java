package com.oriole.oauth2.server.feign;

import com.oriole.common.constant.ResultModel;
import com.oriole.entity.sys.SysUser;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/8/2 22:39
 * description:
 */
@FeignClient("oriole-api")
public interface ISysUserService {

    @GetMapping("/api/sysUser/findByUsername")
    ResultModel<SysUser> findByUsername(@RequestParam("username") String username);
}
