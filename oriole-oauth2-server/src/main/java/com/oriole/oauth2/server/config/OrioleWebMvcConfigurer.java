package com.oriole.oauth2.server.config;

import feign.Request;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/8/2 18:45
 * description: SpringBoot配置类
 */
@Configuration
public class OrioleWebMvcConfigurer implements WebMvcConfigurer {

    @ApiModelProperty(value = "SpringBoot-feign请求超时时间，读取数据超时时间")
    @Bean
    Request.Options feignOptions() {
        return new Request.Options();
    }

    @ApiModelProperty(value = "SpringBoot添加支持CORS跨域访问")
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("*")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedHeaders("*")
                .maxAge(3600);
    }
}
