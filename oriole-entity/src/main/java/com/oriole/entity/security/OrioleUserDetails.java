package com.oriole.entity.security;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/8/1 9:40
 * description: 自定义校验用户
 */
@Setter
@Getter
@ApiModel(value = "自定义Security用户")
public class OrioleUserDetails extends User implements UserDetails {

    private static final long serialVersionUID = -5707910685690688161L;

    @ApiModelProperty(value = "ID")
    private Long id;

    @ApiModelProperty(value = "用户部门")
    private Long deptId;

    public OrioleUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities, Long id, Long deptId) {
        super(username, password, authorities);
        this.id = id;
        this.deptId = deptId;
    }

}
