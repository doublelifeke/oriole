package com.oriole.entity.sys;

import com.oriole.common.constant.ValidEnum;
import com.oriole.entity.common.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/4 20:35
 * Description:
 */
@Entity
@Table(name = "sys_login_record")
@Getter
@Setter
@ApiModel(value = "系统登录记录表")
public class SysLoginRecord extends BaseEntity {

    private static final long serialVersionUID = 1786930229875190846L;

    @ApiModelProperty("主键ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty("登录IP地址")
    @Column(name = "ip")
    private String ip;

    @ApiModelProperty("登录地点")
    @Column(name = "location")
    private String location;

    @ApiModelProperty("浏览器类型")
    @Column(name = "browser")
    private String browser;

    @ApiModelProperty("操作系统")
    @Column(name = "os")
    private String os;

}
