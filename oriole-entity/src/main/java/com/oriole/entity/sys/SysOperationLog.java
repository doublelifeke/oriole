package com.oriole.entity.sys;

import com.oriole.entity.common.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/4 20:54
 * Description:
 */
@Entity
@Table(name = "sys_operation_log")
@Getter
@Setter
@ApiModel(value = "系统操作日志表")
public class SysOperationLog extends BaseEntity {

    private static final long serialVersionUID = 1356324351602160880L;

    @ApiModelProperty("主键ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty("模块标题")
    @Column(name = "title")
    private String title;

    @ApiModelProperty("业务类型")
    @Column(name = "business_type")
    private String businessType;

    @ApiModelProperty("方法名称")
    @Column(name = "method_name")
    private String methodName;

    @ApiModelProperty("请求方式")
    @Column(name = "request_method")
    private String requestMethod;

    @ApiModelProperty("请求URL")
    @Column(name = "url")
    private String url;

    @ApiModelProperty("登录IP地址")
    @Column(name = "ip")
    private String ip;

    @ApiModelProperty("登录地点")
    @Column(name = "location")
    private String location;

    @ApiModelProperty("请求参数")
    @Column(name = "request_params")
    private String requestParams;

    @ApiModelProperty("结果")
    @Column(name = "result")
    private String result;

    @ApiModelProperty("结果")
    @Column(name = "error_msg")
    private String errorMsg;
}
