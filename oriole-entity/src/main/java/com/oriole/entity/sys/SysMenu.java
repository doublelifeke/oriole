package com.oriole.entity.sys;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.oriole.common.constant.ValidEnum;
import com.oriole.entity.common.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/7/30 21:50
 * description:
 */
@Entity
@Table(name = "sys_menu")
@Getter
@Setter
@EqualsAndHashCode(callSuper = false, exclude = {"sysRoles"})
@ApiModel(value = "系统菜单")
public class SysMenu extends BaseEntity {

    private static final long serialVersionUID = -7147757732016871727L;

    @ApiModelProperty("主键ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty("菜单名字")
    @Column(name = "name")
    private String name;

    @ApiModelProperty("菜单URL")
    @Column(name = "url")
    private String url;

    @ApiModelProperty("排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty("父级菜单ID")
    @Column(name = "parent_id")
    private Long parentId;

    @ApiModelProperty("软删除标识，Y/N")
    @Enumerated(value = EnumType.STRING)
    @Column(name = "valid")
    private ValidEnum valid;

    @ApiModelProperty("一个菜单可以被分配给多个角色")
    @JsonIgnore
    @ManyToMany(mappedBy = "sysMenus")
    @OrderBy("id asc")
    private Set<SysRole> sysRoles = new LinkedHashSet<>();

    @ApiModelProperty("用户快捷菜单")
    @JsonIgnore
    @ManyToMany(mappedBy = "sysMenus")
    @OrderBy("id asc")
    private Set<SysUser> sysUsers = new LinkedHashSet<>();
}
