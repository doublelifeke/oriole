package com.oriole.entity.sys;

import com.oriole.entity.common.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/31 14:14
 * Description:
 */
@Entity
@Table(name = "sys_file")
@Getter
@Setter
@ApiModel(value = "系统文件表")
public class SysFile extends BaseEntity {

    private static final long serialVersionUID = 7653622754132419705L;

    @ApiModelProperty("主键ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty("源文件名字")
    @Column(name = "original_name")
    private String originalName;

    @ApiModelProperty("文件大小")
    @Column(name = "file_size")
    private Long fileSize;

    @ApiModelProperty("文件地址")
    @Column(name = "url")
    private String url;

}
