package com.oriole.entity.sys;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.oriole.common.constant.ValidEnum;
import com.oriole.entity.common.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/4 18:05
 * Description:
 */
@Entity
@Table(name = "sys_dictionary_type")
@Getter
@Setter
@EqualsAndHashCode(callSuper = false, exclude = {"sysDictionaryDatas"})
@ApiModel(value = "系统字典类型表")
public class SysDictionaryType extends BaseEntity {

    private static final long serialVersionUID = -5675888474082104880L;

    @ApiModelProperty("主键ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty("字典名字")
    @Column(name = "name")
    private String name;

    @ApiModelProperty("字典类型，unique")
    @Column(name = "type")
    private String type;

    @ApiModelProperty("排序")
    @Column(name = "sort")
    private String sort;

    @ApiModelProperty("备注")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty("软删除标识，Y/N")
    @Enumerated(value = EnumType.STRING)
    @Column(name = "valid")
    private ValidEnum valid;

    @ApiModelProperty("字典数据属于字典类型，一个字典类型有多个字典数据")
    @JsonIgnore
    @OneToMany(mappedBy = "sysDictionaryType")
    @OrderBy("id asc")
    private Set<SysDictionaryData> sysDictionaryDatas = new LinkedHashSet<>();

}
