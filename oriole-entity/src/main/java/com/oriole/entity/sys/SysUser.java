package com.oriole.entity.sys;

import com.oriole.common.constant.ValidEnum;
import com.oriole.entity.common.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * <p>
 * 系统用户表
 * </p>
 *
 * @author hautxxxyzjk@163.com
 * @since 2020-03-10
 */
@Entity
@Table(name = "sys_user")
@Getter
@Setter
@EqualsAndHashCode(callSuper = false, exclude = {"sysRoles", "sysDept"})
@ApiModel(value = "系统用户类")
public class SysUser extends BaseEntity {

    private static final long serialVersionUID = 6124269301594494124L;

    @ApiModelProperty("主键ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty("登录名")
    @Size(min = 5, max = 32)
    @NotNull
    @Column(name = "username")
    private String username;

    @ApiModelProperty("密码")
    @Column(name = "password")
    private String password;

    @ApiModelProperty("头像")
    @Column(name = "avatar")
    private String avatar;

    @ApiModelProperty("是否超级管理员，Y/N")
    @Enumerated(value = EnumType.STRING)
    @Column(name = "is_super")
    private ValidEnum isSuper;

    @ApiModelProperty("软删除标识，Y/N")
    @Enumerated(value = EnumType.STRING)
    @Column(name = "valid")
    private ValidEnum valid;

    @ApiModelProperty("一个用户可以有多个角色")
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @OrderBy("id asc")
    @JoinTable(name = "sys_user_role",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private Set<SysRole> sysRoles = new LinkedHashSet<>();

    @ApiModelProperty("一个用户属于一个部门，一个部门有多个用户")
    @ManyToOne(cascade ={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "dept_id")
    private SysDept sysDept;

    @ApiModelProperty("一个用户可以有多个快捷菜单")
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @OrderBy("id asc")
    @JoinTable(name = "sys_user_shortcut_menu",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "menu_id", referencedColumnName = "id")})
    private Set<SysMenu> sysMenus = new LinkedHashSet<>();

}
