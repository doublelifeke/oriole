package com.oriole.entity.sys;

import com.oriole.common.constant.ValidEnum;
import com.oriole.entity.common.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/4 18:52
 * Description:
 */
@Entity
@Table(name = "sys_dictionary_data")
@Getter
@Setter
@EqualsAndHashCode(callSuper = false, exclude = {"sysDictionaryType"})
@ApiModel(value = "系统字典数据表")
public class SysDictionaryData extends BaseEntity {

    private static final long serialVersionUID = -6287833221746357298L;

    @ApiModelProperty("主键ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty("字典数据属于字典类型，一个字典类型有多个字典数据")
    @ManyToOne(cascade ={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "type_id")
    private SysDictionaryType sysDictionaryType;

    @ApiModelProperty("字典标签")
    @Column(name = "label")
    private String label;

    @ApiModelProperty("字典键值")
    @Column(name = "value")
    private String value;

    @ApiModelProperty("排序")
    @Column(name = "sort")
    private String sort;

    @ApiModelProperty("备注")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty("软删除标识，Y/N")
    @Enumerated(value = EnumType.STRING)
    @Column(name = "valid")
    private ValidEnum valid;

}
