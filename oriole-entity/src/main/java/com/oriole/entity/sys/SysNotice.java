package com.oriole.entity.sys;

import com.oriole.common.constant.ValidEnum;
import com.oriole.entity.common.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/4 20:32
 * Description:
 */
@Entity
@Table(name = "sys_notice")
@Getter
@Setter
@ApiModel(value = "系统通知公告表")
public class SysNotice extends BaseEntity {

    private static final long serialVersionUID = -4069819609472529502L;

    @ApiModelProperty("主键ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty("系统字典数据Id")
    @ManyToOne(cascade ={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "data_id")
    private SysDictionaryData sysDictionaryData;

    @ApiModelProperty("标题")
    @Column(name = "title")
    private String title;

    @ApiModelProperty("内容")
    @Column(name = "content")
    private String content;

    @ApiModelProperty("备注")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty("软删除标识，Y/N")
    @Enumerated(value = EnumType.STRING)
    @Column(name = "valid")
    private ValidEnum valid;

}
