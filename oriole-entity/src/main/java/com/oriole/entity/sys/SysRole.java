package com.oriole.entity.sys;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.oriole.common.constant.ValidEnum;
import com.oriole.entity.common.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/7/30 19:29
 * description:
 */
@Entity
@Table(name = "sys_role")
@Getter
@Setter
@EqualsAndHashCode(callSuper = false, exclude = {"sysUsers", "sysMenus"})
@ApiModel(value = "系统角色类")
public class SysRole extends BaseEntity {

    private static final long serialVersionUID = -8103362725897423796L;

    @ApiModelProperty("主键ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty("角色key")
    @Column(name = "name")
    private String name;

    @ApiModelProperty("角色描述")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty("软删除标识，Y/N")
    @Enumerated(value = EnumType.STRING)
    @Column(name = "valid")
    private ValidEnum valid;

    @ApiModelProperty("一个角色可以被分配为多个用户")
    @JsonIgnore
    @ManyToMany(mappedBy = "sysRoles")
    @OrderBy("id asc")
    private Set<SysUser> sysUsers = new LinkedHashSet<>();

    @ApiModelProperty("一个角色可以有多个菜单")
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @OrderBy("id asc")
    @JoinTable(name = "sys_role_menu",
            joinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "menu_id", referencedColumnName = "id")})
    private Set<SysMenu> sysMenus = new LinkedHashSet<>();
}
