package com.oriole.entity.sys;

import com.oriole.common.constant.ValidEnum;
import com.oriole.entity.common.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/4 19:46
 * Description:
 */
@Entity
@Table(name = "sys_config")
@Getter
@Setter
@ApiModel(value = "系统部配置")
public class SysConfig extends BaseEntity {

    private static final long serialVersionUID = 4700672468985613015L;

    @ApiModelProperty("主键ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty("字典类型Id")
    @Column(name = "type_id")
    private Long typeId;

    @ApiModelProperty("字典标签")
    @Column(name = "config_key")
    private String configKey;

    @ApiModelProperty("字典键值")
    @Column(name = "value")
    private String value;

    @ApiModelProperty("备注")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty("软删除标识，Y/N")
    @Enumerated(value = EnumType.STRING)
    @Column(name = "valid")
    private ValidEnum valid;

}
