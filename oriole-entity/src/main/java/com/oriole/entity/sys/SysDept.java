package com.oriole.entity.sys;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.oriole.common.constant.ValidEnum;
import com.oriole.entity.common.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/8/1 10:33
 * description:
 */
@Entity
@Table(name = "sys_dept")
@Getter
@Setter
@EqualsAndHashCode(callSuper = false, exclude = {"sysUsers"})
@ApiModel(value = "系统部门")
public class SysDept extends BaseEntity {

    private static final long serialVersionUID = -8788931562347172794L;

    @ApiModelProperty("主键ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty("部门名字")
    @Column(name = "name")
    private String name;

    @ApiModelProperty("排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty("父级部门ID")
    @Column(name = "parent_id")
    private String parentId;

    @ApiModelProperty("软删除标识，Y/N")
    @Enumerated(value = EnumType.STRING)
    @Column(name = "valid")
    private ValidEnum valid;

    @ApiModelProperty("一个用户属于一个部门，一个部门有多个用户")
    @JsonIgnore
    @OneToMany(mappedBy = "sysDept")
    @OrderBy("id asc")
    private Set<SysUser> sysUsers = new LinkedHashSet<>();
}
