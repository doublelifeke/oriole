package com.oriole.entity.sys;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.oriole.common.constant.DatePatternConstant;
import com.oriole.entity.common.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/9/20 21:14
 * description:
 */
@Entity
@Table(name = "sys_job_log")
@Getter
@Setter
@ApiModel(value = "定时任务调度日志表")
public class SysJobLog extends BaseEntity {

    private static final long serialVersionUID = -9037116655648719652L;

    @ApiModelProperty("主键ID,日志序号")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty("任务名称")
    @Column(name = "job_name")
    private String jobName;

    @ApiModelProperty("任务组名")
    @Column(name = "job_group")
    private String jobGroup;

    @ApiModelProperty("调用目标字符串")
    @Column(name = "invoke_target")
    private String invokeTarget;

    @ApiModelProperty("日志信息")
    @Column(name = "job_message")
    private String jobMessage;

    @ApiModelProperty("执行状态,0=正常,1=失败")
    @Column(name = "status")
    private String status;

    @ApiModelProperty("异常信息")
    @Column(name = "exception_info")
    private String exceptionInfo;

    @ApiModelProperty("开始时间")
    @Column(name = "start_time")
    @DateTimeFormat(pattern= DatePatternConstant.yyyyMMddHHmmss)
    @JsonFormat(pattern = DatePatternConstant.yyyyMMddHHmmss, timezone = "GMT+8")
    private Date startTime;

    @ApiModelProperty("结束时间")
    @Column(name = "end_time")
    @DateTimeFormat(pattern= DatePatternConstant.yyyyMMddHHmmss)
    @JsonFormat(pattern = DatePatternConstant.yyyyMMddHHmmss, timezone = "GMT+8")
    private Date endTime;

}
