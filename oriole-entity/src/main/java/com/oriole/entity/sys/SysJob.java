package com.oriole.entity.sys;

import com.oriole.common.constant.ScheduleConstants;
import com.oriole.entity.common.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/9/20 21:14
 * description:
 */
@Entity
@Table(name = "sys_job")
@Getter
@Setter
@ApiModel(value = "定时任务调度表")
public class SysJob extends BaseEntity {

    private static final long serialVersionUID = -6789301098970123753L;

    @ApiModelProperty("主键ID,任务序号")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty("任务名称")
    @Column(name = "job_name")
    private String jobName;

    @ApiModelProperty("任务组名")
    @Column(name = "job_group")
    private String jobGroup;

    @ApiModelProperty("调用目标字符串")
    @Column(name = "invoke_target")
    private String invokeTarget;

    @ApiModelProperty("执行表达式")
    @Column(name = "cron_expression")
    private String cronExpression;

    @ApiModelProperty("cron计划策略,0=默认,1=立即触发执行,2=触发一次执行,3=不触发立即执行")
    @Column(name = "misfire_policy")
    private String misfirePolicy;

    @ApiModelProperty("是否并发执行,0=允许,1=禁止")
    @Column(name = "concurrent")
    private String concurrent;

    @ApiModelProperty("任务状态,0=正常,1=暂停")
    @Column(name = "status")
    private String status;

}
