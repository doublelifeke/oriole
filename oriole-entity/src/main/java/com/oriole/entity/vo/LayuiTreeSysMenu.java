package com.oriole.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/12 22:25
 * Description: Layui 树形组件
 * https://www.layui.com/doc/modules/tree.html#data
 */
@Setter
@Getter
@ToString
public class LayuiTreeSysMenu implements Serializable {

    private static final long serialVersionUID = -5486557774228732540L;

    @ApiModelProperty(value = "节点标题", dataType = "String")
    private String title;

    @ApiModelProperty(value = "节点唯一索引值，用于对指定节点进行各类操作", dataType = "String/Number", notes = "任意唯一的字符或数字")
    private Long id;

    @ApiModelProperty(value = "节点字段名", dataType = "String", notes = "一般对应表字段名")
    private String field;

    @ApiModelProperty(value = "子节点。支持设定选项同父节点", dataType = "Array", notes = "[{title: '子节点1', id: '111'}]")
    private ArrayList<LayuiTreeSysMenu> children;

    @ApiModelProperty(value = "点击节点弹出新窗口对应的 url。需开启 isJump 参数", dataType = "String", notes = "任意 URL")
    private String href;

    @ApiModelProperty(value = "节点是否初始展开，默认 false", dataType = "Boolean", notes = "true")
    private Boolean spread = Boolean.FALSE;

    @ApiModelProperty(value = "节点是否初始为选中状态（如果开启复选框的话），默认 false", dataType = "Boolean", notes = "true")
    private Boolean checked = Boolean.FALSE;

    @ApiModelProperty(value = "节点是否为禁用状态。默认 false", dataType = "Boolean", notes = "false")
    private Boolean disabled = Boolean.FALSE;

}
