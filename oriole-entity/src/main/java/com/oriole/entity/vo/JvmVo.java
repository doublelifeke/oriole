package com.oriole.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/13 21:12
 * Description:
 */
@Data
@ApiModel(value = "JVM相关信息")
public class JvmVo {

    @ApiModelProperty("当前JVM占用的内存总数(M)")
    private double total;

    @ApiModelProperty("JVM最大可用内存总数(M)")
    private double max;

    @ApiModelProperty("JVM空闲内存(M)")
    private double free;

    @ApiModelProperty("JVM已用内存(M)")
    private double used;

    @ApiModelProperty("使用率")
    private double useRate;

    @ApiModelProperty("JDK版本")
    private String jdkVersion;

    @ApiModelProperty("JDK路径")
    private String jdkHome;

    @ApiModelProperty("JDK名称")
    private String jdkName;

    @ApiModelProperty("JDK启动时间")
    private String jdkStartTime;

    @ApiModelProperty("JDK运行时间")
    private String jdkRunTime;

}
