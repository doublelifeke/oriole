package com.oriole.entity.vo;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/4 23:14
 * Description:
 */
@ApiModel(value = "系统配置信息VO类")
@Setter
@Getter
@ToString
public class SysConfigVo implements Serializable {

    private static final long serialVersionUID = 4437159741556727149L;

    private String sysName;

    private String sysLogo;

    private String sysBottomText;

    private String sysColor;

    private String sysIndex;

}
