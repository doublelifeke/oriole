package com.oriole.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/13 21:10
 * Description:
 */
@Data
@ApiModel(value = "CPU相关信息")
public class CpuVo {

    @ApiModelProperty("核心数")
    private int cpuNum;

    @ApiModelProperty("CPU总的使用率")
    private double total;

    @ApiModelProperty("CPU系统使用率")
    private double sys;

    @ApiModelProperty("CPU用户使用率")
    private double used;

    @ApiModelProperty("CPU当前等待率")
    private double wait;

    @ApiModelProperty("CPU当前空闲率")
    private double free;

}
