package com.oriole.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/13 21:16
 * Description:
 */
@Data
@ApiModel("系统硬盘相关信息")
public class HardDiskVo {

    @ApiModelProperty("盘符路径")
    private String dirName;

    @ApiModelProperty("盘符类型")
    private String sysTypeName;

    @ApiModelProperty("文件类型")
    private String typeName;

    @ApiModelProperty("总大小")
    private String total;

    @ApiModelProperty("剩余大小")
    private String free;

    @ApiModelProperty("已经使用量")
    private String used;

    @ApiModelProperty("资源的使用率")
    private double usage;

}
