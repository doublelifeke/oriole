package com.oriole.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/13 21:14
 * Description:
 */
@Data
@ApiModel("系统相关信息")
public class SystemVo {

    @ApiModelProperty("服务器名称")
    private String computerName;

    @ApiModelProperty("服务器Ip")
    private String computerIp;

    @ApiModelProperty("项目路径")
    private String userDir;

    @ApiModelProperty("操作系统")
    private String osName;

    @ApiModelProperty("系统架构")
    private String osArch;

}
