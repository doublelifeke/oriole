package com.oriole.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/8/31 22:50
 * description: 系统监控信息VO
 */
@Data
@ApiModel(value = "系统监控信息VO")
public class MonitorVo implements Serializable {

    private static final long serialVersionUID = -235610257955711035L;

    @ApiModelProperty("CPU相关信息")
    private CpuVo cpuVo = new CpuVo();

    @ApiModelProperty("內存相关信息")
    private MemoryVo memoryVo = new MemoryVo();

    @ApiModelProperty("JVM相关信息")
    private JvmVo jvmVo = new JvmVo();

    @ApiModelProperty("服务器相关信息")
    private SystemVo systemVo = new SystemVo();

    @ApiModelProperty("磁盘相关信息")
    private List<HardDiskVo> hardDiskVos = new LinkedList<>();
}
