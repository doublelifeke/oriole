package com.oriole.entity.vo;

import com.oriole.common.constant.ValidEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/9/28 20:10
 * Description:
 */
@ApiModel(value = "系统角色VO类")
@Setter
@Getter
@ToString
public class SysMenuVo implements Serializable {

    private static final long serialVersionUID = -2992952896042821982L;

    @ApiModelProperty("主键ID")
    private Long id;

    @ApiModelProperty("菜单名字")
    private String name;

    @ApiModelProperty("菜单URL")
    private String url;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("父级菜单ID")
    private Long parentId;

    @ApiModelProperty(value = "子节点的集合")
    private List<SysMenuVo> children = new ArrayList<>();

}
