package com.oriole.entity.vo;

import com.oriole.common.constant.ValidEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/4 21:38
 * Description:
 */
@ApiModel(value = "系统用户基本信息VO类")
@Setter
@Getter
@ToString
public class SysUserVo implements Serializable {

    private static final long serialVersionUID = -6504306228088238842L;

    @ApiModelProperty("主键ID")
    private Long id;

    @ApiModelProperty("登录名")
    private String username;

    @ApiModelProperty("头像")
    private String avatar;

    @ApiModelProperty("是否超级管理员，Y/N")
    @Enumerated(value = EnumType.STRING)
    private ValidEnum isSuper;

    @ApiModelProperty("软删除标识，Y/N")
    @Enumerated(value = EnumType.STRING)
    private ValidEnum valid;
}
