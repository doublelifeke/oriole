package com.oriole.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/11/13 21:13
 * Description:
 */
@Data
@ApiModel("內存相关信息")
public class MemoryVo {

    @ApiModelProperty("内存总量")
    private double total;

    @ApiModelProperty("已用内存")
    private double used;

    @ApiModelProperty("剩余内存")
    private double free;

    @ApiModelProperty("使用率")
    private double useRate;

}
