package com.oriole.entity.vo;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelifeke
 * Email: hautxxxyzjk@163.com
 * DateTime: 2020/10/5 22:08
 * Description:
 */
@ApiModel(value = "系统用户修改密码VO类")
@Setter
@Getter
@ToString
public class SysUserPasswordVo implements Serializable {

    private static final long serialVersionUID = 2288316152609750210L;

    @NotNull
    private String oldPassword;

    @NotNull
    @Min(value = 6)
    @Max(value = 16)
    private String password;

    @NotNull
    @Min(value = 6)
    @Max(value = 16)
    private String rePassword;
    
}
