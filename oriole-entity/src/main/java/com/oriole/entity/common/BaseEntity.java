package com.oriole.entity.common;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.oriole.common.constant.DatePatternConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 *
 * @author doublelife
 * Date: 2020/7/29 22:54
 * description:
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Setter
@Getter
@ApiModel(value = "JPA审计类")
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 3117602695605974504L;

    @CreatedBy
    @ApiModelProperty("创建人ID")
    @Column(name = "created_by")
    private Long createdBy;

    @CreatedDate
    @ApiModelProperty("创建时间")
    @Column(name = "created_date")
    @DateTimeFormat(pattern= DatePatternConstant.yyyyMMddHHmmss)
    @JsonFormat(pattern = DatePatternConstant.yyyyMMddHHmmss, timezone = "GMT+8")
    private Date createdDate;

    @LastModifiedBy
    @ApiModelProperty("更新人ID")
    @Column(name = "last_modified_by")
    private Long lastModifiedBy;

    @LastModifiedDate
    @ApiModelProperty("更新时间")
    @Column(name = "last_modified_date")
    @DateTimeFormat(pattern= DatePatternConstant.yyyyMMddHHmmss)
    @JsonFormat(pattern = DatePatternConstant.yyyyMMddHHmmss, timezone = "GMT+8")
    private Date lastModifiedDate;

}
