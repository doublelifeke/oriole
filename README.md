oriole
====

介绍
----
    通用后台管理系统，基于springboot开发的脚手架，采用分模块的方式便于开发和维护,为快速开发后台系统而生的脚手架。
        
使用技术
----
    后端技术：SpringBoot + Spring Cloud + Spring Data Jpa + Spring Security + Jwt
    前端技术：Layui + Jquery + Thymeleaf
    
    springboot  --> 2.2.5.RELEASE
    springcloud --> Hoxton.RELEASE
    quartz --> 2.3.2
    liquibase --> 3.8.7（用于跟踪、管理和应用数据库变化的开源的数据库重构工具）
    swagger + Knife4j --> 2.9.2（接口文档）
    screw --> 1.0.4（数据库文档生成工具）
    lombok --> 1.18.8
    querydsl --> 4.2.2

主要功能
----
    + 系统管理
        + 菜单管理：完成
        + 角色管理：完成
        + 部门管理：完成
        + 用户管理：完成
        + 日志管理：完成
        + 字典管理：完成
        + 系统配置：完成
        + 通知公告：完成
    + 系统监控
        + 在线用户：完成
        + 定时任务：完成
        + 数据监控：完成
        + 服务监控：完成
    + 系统工具
        + 系统接口：完成
        + 代码生成
        
预览效果图
----

登录页：
![login.jpg](https://gitee.com/doublelifeke/upload/raw/master/oriole/login.jpg "login")

首页：
![index.jpg](https://gitee.com/doublelifeke/upload/raw/master/oriole/index.jpg "index")

服务监控：
![monitor.jpg](https://gitee.com/doublelifeke/upload/raw/master/oriole/monitor.jpg "monitor")

密码修改：
![changepassword.jpg](https://gitee.com/doublelifeke/upload/raw/master/oriole/changepassword.jpg "changepassword")

定时任务：
![sysJob.jpg](https://gitee.com/doublelifeke/upload/raw/master/oriole/sysJob.jpg "sysJob")

角色管理-新增：
![sysMenu_save.jpg](https://gitee.com/doublelifeke/upload/raw/master/oriole/sysMenu_save.jpg "sysMenu_save")



其他
----   
